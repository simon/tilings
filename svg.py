# Simple code to output SVG diagrams.

import html

class SVG:
    def __init__(self, scale, fontscale=1, edgelabelscale=1, blobscale=1):
        self.xmin = self.xmax = self.ymin = self.ymax = None
        self.bounds_forced = False
        self.polygons = []
        self.circles = []
        self.text = []
        self.clippath = None
        self.scale = scale
        self.fontscale = fontscale
        self.edgelabelscale = edgelabelscale
        self.blobscale = blobscale
        self.extra_css = ""

    def add_css(self, s):
        self.extra_css += s

    def update_bounds(self, z):
        if self.bounds_forced:
            return
        self.xmin = z.real if self.xmin is None else min(z.real, self.xmin)
        self.xmax = z.real if self.xmax is None else max(z.real, self.xmax)
        self.ymin = z.imag if self.ymin is None else min(z.imag, self.ymin)
        self.ymax = z.imag if self.ymax is None else max(z.imag, self.ymax)

    def force_bounds(self, x0, y0, x1, y1):
        self.xmin, self.xmax = sorted([x0, x1])
        self.ymin, self.ymax = sorted([y0, y1])
        self.bounds_forced = True

    def share_bounds(self, *others):
        for other in others:
            self.update_bounds(other.xmin + 1j * other.ymin)
            self.update_bounds(other.xmax + 1j * other.ymax)
        for other in others:
            other.update_bounds(self.xmin + 1j * self.ymin)
            other.update_bounds(self.xmax + 1j * self.ymax)

    @property
    def bounds(self):
        return self.xmin, self.ymin, self.xmax, self.ymax

    def add_polygon(self, vertices, labelpos=None, label=None, cssclass=None,
                    edgelabel_sign=0, edgelabel_text=None, data=None):
        for v in vertices:
            self.update_bounds(complex(v))
        self.polygons.append([vertices, labelpos, label, cssclass,
                              edgelabel_sign, edgelabel_text, True, data])

    def add_path(self, vs, cssclass="thick", data=None):
        for v in vs:
            self.update_bounds(complex(v))
        self.polygons.append([vs, None, None, cssclass,
                              None, None, False, data])

    def clip_to_path(self, vs):
        self.clippath = vs

    def add_thick_line(self, v1, v2):
        self.add_thick_path([v1, v2], "thick")

    def add_arrow(self, base, tip):
        base = complex(base)
        tip = complex(tip)
        unit = tip - base
        s = 0.15j * unit
        h = 0.45j * unit
        e = 0.45 * unit
        self.add_polygon([
            tip, tip+h-e, tip+s-e, base+s, base-s, tip-s-e, tip-h-e
        ], cssclass="arrow")

    def add_vertex_blob(self, centre, large):
        radius = 0.2 if large else 0.1
        z = complex(centre)
        self.update_bounds(z+radius*(1+1j))
        self.update_bounds(z-radius*(1+1j))
        self.circles.append([centre, radius, None])

    def add_circle(self, centre, radius, cssclass):
        z = complex(centre)
        self.update_bounds(z+radius*(1+1j))
        self.update_bounds(z-radius*(1+1j))
        self.circles.append([centre, radius, cssclass])

    def add_text(self, pos, size, text, cssclass=None):
        self.text.append([pos, size, text, cssclass])

    def write(self, fh, expand_for_exterior_text=True):
        if not self.bounds_forced:
            self.xmax += 0.2 * self.fontscale
            self.xmin -= 0.2 * self.fontscale
            self.ymax += 0.2 * self.fontscale
            self.ymin -= 0.2 * self.fontscale

        width = self.scale * (self.xmax - self.xmin)
        height = self.scale * (self.ymax - self.ymin)
        stroke = 1.5
        if not self.bounds_forced:
            xmargin = stroke/2
            ymargin = stroke/2
            if expand_for_exterior_text:
                xmargin += self.scale * 0.5
                ymargin += self.scale * 0.2
            width += xmargin*2
            height += ymargin*2
        else:
            xmargin = ymargin = 0

        # Functions to get the SVG x and y coordinates of a point.
        # Opposite ways round because SVG's y-axis runs downwards
        # rather than upwards.
        xcoord = lambda z: self.scale * (z.real - self.xmin) + xmargin
        ycoord = lambda z: self.scale * (self.ymax - z.imag) + ymargin

        def draw_text(pos, fontsize, text, cssclass=None):
            fontsize *= self.fontscale
            pos = complex(pos)
            x = xcoord(pos)
            y = ycoord(pos) + 0.35 * fontsize
            fh.write(f'<text style="font-size: {fontsize}px" ')
            if cssclass is not None:
                fh.write(f'class="{cssclass}" ')
            fh.write(f'x="{x}" y="{y}">{text}</text>\n')

        fh.write(f'''\
<?xml version="1.0" encoding="UTF-8" standalone="no"?>
<svg xmlns="http://www.w3.org/2000/svg" version="1.1"
     width="{width}" height="{height}">
<style type="text/css">
path {{
    stroke: black;
    fill: none;
    stroke-width: 1.5;
    stroke-linejoin: round;
    stroke-linecap: round;
}}
path.arrow {{
    stroke: none;
    fill: rgba(0, 0, 0, 0.2);
}}
path.thick {{
    stroke-width: 12;
}}
text {{
    fill: black;
    font-family: Sans;
    text-anchor: middle;
    text-align: center;
}}
{self.extra_css}</style>
''')

        path_extra_attrs = ''
        def write_path(vertices, cssclass, close, data=None):
            fh.write('<path')
            if cssclass is not None:
                fh.write(f' class="{cssclass}"')
            if data is not None:
                for key, value in sorted(data.items()):
                    fh.write(f' data-{key}="{html.escape(str(value))}"')
            fh.write(path_extra_attrs)
            fh.write(' d="')
            cmd = "M"
            for v in vertices:
                z = complex(v)
                fh.write(f'{cmd} {xcoord(z)} {ycoord(z)} ')
                cmd = "L"
            if close:
                fh.write(f'z')
            fh.write(f'"/>\n')

        if self.clippath is not None:
            fh.write(f'<defs>\n<clipPath id="clip">')
            write_path(self.clippath, None, True)
            fh.write('</clipPath></defs>\n')
            path_extra_attrs += ' clip-path="url(#clip)"'

        for (vertices, labelpos, label, cssclass, edgelabel_sign,
             edgelabel_text, close, data) in self.polygons:
            write_path(vertices, cssclass, close, data)

            if edgelabel_sign:
                edgelabel_offset = 0.2j * self.edgelabelscale * edgelabel_sign
                n = len(vertices)
                for i in range(n):
                    z1, z2 = complex(vertices[i]), complex(vertices[(i+1) % n])
                    uvec = z1-z2
                    uvec /= abs(uvec)
                    draw_text(0.5 * (z1+z2) + edgelabel_offset * uvec,
                              self.scale * 0.2, edgelabel_text(i))

            if label is not None:
                draw_text(labelpos, self.scale * 0.3, label)

        for centre, radius, cssclass in self.circles:
            radius *= self.blobscale
            z = complex(centre)
            fh.write(f'<circle cx="{xcoord(z)}" cy="{ycoord(z)}" '
                     f'r="{self.scale * radius}"')
            if cssclass is not None:
                fh.write(f' class="{cssclass}"')
            fh.write('/>\n')

        for pos, size, text, cssclass in self.text:
            draw_text(pos, self.scale * size, text, cssclass)

        fh.write('</svg>\n')
