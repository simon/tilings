# Disjoint-set forest, aka union-find data structure.

import unittest

class DSF:
    """Incrementally tracks equivalences between an arbitrary set of
    objects.

    Objects are initially all considered distinct; the 'unify'
    operation causes two of them to be regarded as the same. The
    'equivalent' query asks whether two are currently known to be the
    same (via any transitive path of previous unifications).

    'class_of' returns an iterator that lists all elements equivalent
    to the input one. 'classes' returns an iterator delivering all
    such iterators one by one.

    The full set of objects need not be specified in advance; anything
    previously unknown is considered to be in a class by itself.
    However, you can register an item deliberately via register(), to
    cause its singleton class to show up in the classes() iterator.

    """

    def __init__(self):
        self.parent = {}
        self.siblings = {}

    def _findroot(self, item):
        while True:
            parent = self.parent.get(item, item)
            if parent == item:
                return parent
            item = parent

    def _pathcompress(self, item, root):
        while item != root:
            parent = self.parent.get(item, item)
            self.parent[item] = root
            item = parent

    def unify(self, x, y):
        xroot = self._findroot(x)
        yroot = self._findroot(y)
        if xroot != yroot:
            xsibs = self.siblings.get(xroot, [xroot])
            ysibs = self.siblings.get(yroot, [yroot])
            newroot = xroot if len(xsibs) > len(ysibs) else yroot
            self.parent[xroot] = self.parent[yroot] = newroot
            try:
                del self.siblings[xroot]
            except KeyError:
                pass
            try:
                del self.siblings[yroot]
            except KeyError:
                pass
            self.siblings[newroot] = xsibs + ysibs
            toret = True
        else:
            newroot = xroot
            toret = False
        self._pathcompress(x, newroot)
        self._pathcompress(y, newroot)
        return toret

    def unify_all(self, items):
        toret = False
        items = iter(items)
        try:
            prev = next(items)
        except StopIteration:
            return toret
        for item in items:
            if self.unify(prev, item):
                toret = True
            prev = item
        return toret

    def canonical(self, x):
        xroot = self._findroot(x)
        self._pathcompress(x, xroot)
        return xroot

    def equivalent(self, x, y):
        return self.canonical(x) == self.canonical(y)

    def register(self, *args):
        for x in args:
            if x not in self.parent:
                self.parent[x] = x
                self.siblings[x] = [x]

    def class_of(self, x):
        xroot = self.canonical(x)
        xsibs = self.siblings.get(xroot, [xroot])
        return iter(xsibs)

    def classes(self):
        return iter(self.siblings.values())
    def sets(self):
        return set(map(frozenset, self.classes()))

class DSFTests(unittest.TestCase):
    def testTransitivity(self):
        dsf = DSF()
        self.assertFalse(dsf.equivalent('a', 'b'))
        dsf.unify('a', 'b')
        self.assertTrue(dsf.equivalent('a', 'b'))
        self.assertFalse(dsf.equivalent('a', 'd'))
        self.assertFalse(dsf.equivalent('c', 'd'))
        dsf.unify('c', 'd')
        self.assertTrue(dsf.equivalent('a', 'b'))
        self.assertTrue(dsf.equivalent('c', 'd'))
        self.assertFalse(dsf.equivalent('a', 'd'))
        dsf.unify('b', 'c')
        self.assertTrue(dsf.equivalent('a', 'd'))

    def testClassOf(self):
        dsf = DSF()
        self.assertEqual(set(dsf.class_of('a')), {'a'})
        dsf.unify('a', 'b')
        self.assertEqual(set(dsf.class_of('a')), {'a','b'})
        self.assertEqual(set(dsf.class_of('b')), {'a','b'})
        dsf.unify('c', 'd')
        self.assertEqual(set(dsf.class_of('a')), {'a','b'})
        self.assertEqual(set(dsf.class_of('b')), {'a','b'})
        self.assertEqual(set(dsf.class_of('c')), {'c','d'})
        self.assertEqual(set(dsf.class_of('d')), {'c','d'})
        dsf.unify('a', 'd')
        self.assertEqual(set(dsf.class_of('a')), {'a','b','c','d'})
        self.assertEqual(set(dsf.class_of('b')), {'a','b','c','d'})
        self.assertEqual(set(dsf.class_of('c')), {'a','b','c','d'})
        self.assertEqual(set(dsf.class_of('d')), {'a','b','c','d'})

    def testClasses(self):
        dsf = DSF()
        self.assertEqual(dsf.sets(), set())
        dsf.register('a')
        self.assertEqual(dsf.sets(), {frozenset({'a'})})
        dsf.register('b')
        self.assertEqual(dsf.sets(), {frozenset({'a'}), frozenset({'b'})})
        dsf.register('b')
        self.assertEqual(dsf.sets(), {frozenset({'a'}), frozenset({'b'})})
        dsf.unify('a','b')
        self.assertEqual(dsf.sets(), {frozenset({'a','b'})})
        dsf.unify('c','d')
        self.assertEqual(dsf.sets(),
                         {frozenset({'a','b'}),frozenset({'c','d'})})
        dsf.register('c')
        self.assertEqual(dsf.sets(),
                         {frozenset({'a','b'}),frozenset({'c','d'})})
        dsf.unify('c','a')
        self.assertEqual(dsf.sets(), {frozenset({'a','b','c','d'})})
