# -*- python -*-

# Code to actually draw tilings.

import collections

import analysis
import util

class UnboundedTargetArea:
    def accept(self, points):
        return True

class Rectangle:
    def __init__(self, x0, y0, x1, y1):
        self.x0, self.x1 = sorted([x0, x1])
        self.y0, self.y1 = sorted([y0, y1])

    def accept(self, points):
        # We can't just check whether all the actual _points_ are in
        # the rectangle, because it's possible that a polygon with all
        # its vertices outside the rectangle might still have an edge
        # crossing one corner. So instead we exclude a polygon only if
        # all its points are on the same side of one of the
        # (infinitely extended) edges of the rectangle.
        #
        # Since the polygons we expect to deal with here are bounded
        # in size, this should be good enough, with the occasional
        # false positive at a corner that won't cost too much.

        xs = [p.real() for p in points]
        if all(x < self.x0 for x in xs) or all(x > self.x1 for x in xs):
            return False

        ys = [p.imag() for p in points]
        if all(y < self.y0 for y in ys) or all(y > self.y1 for y in ys):
            return False

        return True

    def area(self):
        return abs((self.x1 - self.x0) * (self.y1 - self.y0))

    def bounds(self):
        return (self.x0, self.y0, self.x1, self.y1)

class Diagram:
    def __init__(self, tiling, layer_name, target_area):
        self.tiling = tiling
        self.layer = self.tiling.layers_by_name[layer_name]
        self.expansion = self.tiling.expansions[
            self.layer.successor, self.layer.name]
        self.target_area = target_area
        self.geom_to_comb = {}
        self.queue = []
        self.boundary_segments = collections.defaultdict(set)

    def estimate_num_tiles(self):
        # Requires the target area to provide a .area() method;
        # requires one tile to have been added to the tiling already.

        # Find the limiting probabilities of the tile types in this tiling
        an = analysis.Analysis(self.tiling)
        probs = an.limiting_probabilities()
        probs = probs[self.layer.name]

        # Find the expected mean area of a prototype polygon for the
        # tiling
        mean_proto_area = sum(self.tiling.tile_polygon(tile).area() * prob
                              for tile, prob in probs.items())

        # Determine the scale factor between the area of the tiling's
        # prototype polygons and the ones we're drawing here
        polygon, coords = next(iter(self.geom_to_comb.items()))
        scale = polygon.area() / (
            self.tiling.tile_polygon(coords.base_type).area())

        # Adjust our mean area for the actual output size of the tiles
        mean_area = mean_proto_area * scale

        # And divide the target area by that
        return int(ceil(self.target_area.area() / mean_area))

    def add_tile(self, polygon, coords):
        if polygon not in self.geom_to_comb:
            self.geom_to_comb[polygon] = coords
            self.queue.append((polygon, coords))
            return True
        return False

    def add_boundary_segment(self, polygon, edge):
        v0, v1 = polygon.vertex(edge), polygon.vertex(edge+1)
        self.boundary_segments[v0].add(v1)
        self.boundary_segments[v1].add(v0)

    def generate(self, boundary_on_exception=False, report_edge=None,
                 report_progress=None):
        ndrawn = 0
        while len(self.queue) > 0:
            old_polygon, old_coords = self.queue.pop(0)
            for old_edge in range(len(self.tiling.tile_edges[
                    old_coords.base_type])):
                new_coords = old_coords.clone()
                boundary = lambda: self.add_boundary_segment(
                    old_polygon, old_edge)
                try:
                    new_edge = new_coords.step(old_edge, boundary)
                except (util.InextensibleAddressException,
                        util.InfiniteAmbiguityException):
                    # Couldn't step off this edge, so move on to the next
                    if boundary_on_exception:
                        boundary()
                    continue
                new_polygon = self.tiling.tile_polygon(new_coords.base_type)
                new_polygon.place_next_to_edge(new_edge, old_polygon, old_edge)
                if self.target_area.accept(new_polygon.vertices):
                    is_new = self.add_tile(new_polygon, new_coords)
                    if report_edge is not None:
                        report_edge(old_polygon, old_coords, old_edge,
                                    new_polygon, new_coords, new_edge)
                    if is_new and report_progress is not None:
                        ndrawn += 1
                        report_progress(ndrawn)

    def boundary_paths(self):
        paths = []
        seen = set() # record one end of each path we've seen

        for v, vsuccs in self.boundary_segments.items():
            if len(vsuccs) != 2:
                # Start a path in every direction from every vertex
                # that doesn't have degree 2
                for w in vsuccs:
                    if (v, w) in seen:
                        continue
                    path = [v, w]
                    prev = v
                    while True:
                        wsuccs = self.boundary_segments[w]
                        if len(wsuccs) != 2:
                            break # this path is done
                        wsuccs = [ws for ws in wsuccs if ws != prev]
                        assert len(wsuccs) == 1
                        prev, w = w, next(iter(wsuccs))
                        path.append(w)
                    seen.add((path[-1], path[-2]))
                    paths.append(path)

        return paths
