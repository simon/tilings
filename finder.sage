# -*- python -*-

import collections
import itertools
import sys

import recursive

class CoordinateNFA:
    """Class that holds an NFA matching a set of possible infinite
    coordinate strings.

    Intended as a method of determining a set of coordinates, in a
    particular substitution system, compatible with a finite patch of
    tiles you got from somewhere else, such as a different
    substitution system, or from some source that didn't give them any
    kind of coordinate labels or metadata at all, just the raw tile
    positions.

    To find coordinates compatible with such a patch, you'd choose a
    starting tile, and make a starting CoordinateNFA that describes
    _all_ the possible coordinate strings for that tile type, using
    the valid_for_tile() class method.

    Then you walk from tile to tile around the patch, and at each
    step, call valid_neighbour() to create a CoordinateNFA matching
    the new tile, which describes all of its possible coordinate
    strings consistent with the NFA you came from.

    At every stage, the idea is that if you pick any coordinate string
    matched by the current NFA, and generate a fresh patch of tiling
    using that as a starting point, it should agree with every tile
    you've so far visited.

    Once you've visited every tile in the patch, walk back to the tile
    you actually _wanted_ to start from, and now you have an NFA that
    shows you every possible coordinate string it might have,
    consistent with the rest of the patch.

    For example, suppose all you knew was that you had a set of
    Penrose half-tile triangles of types A, V and U, with V's edge #2
    meeting A's edge #0, and U and V connected by their #0 edges. Then
    you might do something like this (assuming that 'tiling' was a
    tiling loaded from p2-triangles.tl and 'am' was an
    AdjacencyMatcher built from it):

    a = CoordinateNFA.valid_for_tile(tiling, 'A')
    v = a.valid_neighbour(am, 0, 'V', 2)
    u = v.valid_neighbour(am, 0, 'U', 0)

    and now 'u' contains all the possible coordinate strings that
    describe a U which has those other two tile types in the right
    relationship to it.

    """
    def __init__(self):
        self.state_indices = {} # id -> index
        self.state_descriptions = [] # index -> id
        self.trans = collections.defaultdict(set) # src -> set of (sym, dst)
        self.state_generator = itertools.count(1)

    def discover_state(self, data, queue=[]):
        try:
            return self.state_indices[data]
        except KeyError:
            state = len(self.state_descriptions)
            self.state_descriptions.append(data)
            self.state_indices[data] = state
            queue.append(state)
            return state

    def add_transition(self, src, dst, sym):
        self.trans[src].add((sym, dst))

    @classmethod
    def valid_for_tile(cls, tiling, tile, base_layer=None):
        if base_layer is None:
            base_layer = tiling.layers[0]

        out = cls()
        out.tiling = tiling
        out.base_layer = base_layer
        out.start = out.discover_state(None)
        out.add_transition(out.start, out.discover_state(
            (base_layer.name, tile)), tile)

        layers_done = set()
        layer = base_layer
        while layer.name not in layers_done:
            layers_done.add(layer.name)
            succ = tiling.layers_by_name[layer.successor]
            exp = tiling.expansions[succ.name, layer.name]
            for ptile, children in exp.tiles.items():
                pstate = out.discover_state((succ.name, ptile))
                for cindex, (ctile, *_) in children.items():
                    cstate = out.discover_state((layer.name, ctile))
                    out.add_transition(cstate, pstate, (ptile, cindex))
            layer = succ

        return out.cleanup()

    def explore(self, start, transitions):
        queue = []
        self.start = self.discover_state(start, queue)
        while len(queue) > 0:
            src = queue.pop(0)
            for sym, dst_desc in transitions(self.state_descriptions[src]):
                dst = self.discover_state(dst_desc, queue)
                #print("    discovered", dst_desc, "->", dst)
                self.add_transition(src, dst, sym)
                #print("    added", src, "->", dst, "on", sym)

    def valid_neighbour(self, adjmatcher, src_edge, dst_tile, dst_edge):
        def transitions(src):
            dsrc, initial, amsrc = src
            #print("trans from", dsrc, initial, amsrc)
            for sym, ddst in self.trans[dsrc]:
                if initial:
                    # In the start state, suffix src_edge to the base tile type
                    sym = (sym, src_edge)
                #print("  to am", sym)
                for (sym1,sym2),amdst in adjmatcher.transitions[amsrc]:
                    #print("    from am", sym1, sym2, amdst)
                    if sym1 != sym:
                        continue
                    if initial:
                        if sym2 != (dst_tile, dst_edge):
                            # In the start state, exclude transitions to
                            # the wrong tile type or edge
                            continue
                        sym2 = sym2[0] # strip off the edge
                    #print("      aha", sym2, (ddst, False, amdst))
                    yield sym2, (ddst, False, amdst)

        out = type(self)()
        out.tiling = self.tiling
        out.base_layer = self.base_layer
        out.explore((self.start, True, adjmatcher.start), transitions)
        return out.cleanup()

    def cleanup(self):
        #print("=== before cleanup:")
        #self.dump()
        #print("=== cleanup start")
        deadends = set()
        while True:
            oldlen = len(deadends)
            for src in range(len(self.state_descriptions)):
                is_deadend = True
                for sym, dst in self.trans[src]:
                    if dst not in deadends:
                        is_deadend = False
                if is_deadend:
                    deadends.add(src)
            if len(deadends) == oldlen:
                break
        #print("deadends = ", deadends)

        equiv = {s: (-1 if s in deadends else 0)
                 for s in range(len(self.state_descriptions))}
        #print("equiv = ", equiv)
        while True:
            new_equiv = {}
            equiv_map = {}
            equiv_first = {}
            for src in range(len(self.state_descriptions)):
                if src in deadends:
                    new_equiv[src] = -1
                    continue
                key = frozenset((sym, equiv[dst])
                                for sym, dst in self.trans[src])
                if key not in equiv_map:
                    equiv_map[key] = len(equiv_map)
                    equiv_first[equiv_map[key]] = src
                new_equiv[src] = equiv_map[key]
            #print("equiv = ", new_equiv)
            if new_equiv == equiv:
                break
            equiv = new_equiv

        def transitions(esrc):
            if esrc == -1:
                return
            for sym, dst in self.trans[equiv_first[esrc]]:
                edst = equiv[dst]
                if edst != -1:
                    yield sym, edst
        out = type(self)()
        out.tiling = self.tiling
        out.base_layer = self.base_layer
        out.explore(equiv[self.start], transitions)
        #print("=== after cleanup:")
        #out.dump()
        #print("=== cleanup end")
        return out

    def determinise(self):
        """Make a deterministic version of the machine, for ease of reading
        the output."""
        clean = self.cleanup()

        def transitions(srcset):
            bysym = collections.defaultdict(set)
            for src in srcset:
                for sym, dst in clean.trans[src]:
                    bysym[sym].add(dst)
            for sym, dstset in bysym.items():
                yield sym, frozenset(dstset)

        out = type(self)()
        out.tiling = self.tiling
        out.base_layer = self.base_layer
        out.explore(frozenset({clean.start}), transitions)
        return out.cleanup()

    def empty(self):
        return len(self.trans[self.start]) == 0

    def dump(self, fh=sys.stdout):
        """Dump the whole NFA for a user to examine."""
        for state, desc in enumerate(self.state_descriptions):
            print(f"State {state} # {desc}", file=fh)
            for sym, dst in self.trans[state]:
                print(f"  {sym} -> {dst}", file=fh)

    def known_coords(self):
        """Output any initial list of coordinates that are 'known', in the
        sense that the NFA can't match any sequence that doesn't start
        with those.
        """
        syms = []
        states = {self.start}
        while True:
            poss = collections.defaultdict(set)
            for src in states:
                for sym, dst in self.trans[src]:
                    poss[sym].add(dst)
            if len(poss) != 1:
                return syms
            sym, states = next(iter(poss.items()))
            syms.append(sym)

    def try_find_rho(self, ninit, nrep):
        queue = []
        visited = set()
        def discover(state, index, syms, prevstates):
            key = state, index, tuple(syms), tuple(prevstates)
            if key in visited:
                return
            visited.add(key)
            queue.append((state, index, syms, prevstates))
        discover(self.start, 0, [None] * (ninit + nrep), [None] * nrep)
        #print("rho", ninit, nrep)
        outputs = set()
        while len(queue) > 0:
            entry = queue.pop(0)
            src, index, syms, prevstates = entry
            #print("  try", src, index, syms, prevstates)
            for sym, dst in self.trans[src]:
                src, index, syms, prevstates = entry
                if syms[index] is None:
                    syms = syms.copy()
                    syms[index] = sym
                if syms[index] != sym:
                    continue
                if index >= ninit:
                    rindex = index - ninit
                    if prevstates[rindex] == dst:
                        outputs.add((tuple(syms[:ninit]), tuple(syms[ninit:])))
                    prevstates = prevstates.copy()
                    prevstates[rindex] = dst
                index += 1
                if index == ninit + nrep:
                    index = ninit
                #print("    ->", dst, index, syms, prevstates)
                discover(dst, index, syms, prevstates)
        return outputs

    def simplest_rho(self):
        for info in itertools.count(1):
            for nrep in range(1, info+1):
                for ninit in range(info-nrep+1):
                    outputs = self.try_find_rho(ninit, nrep)
                    if len(outputs) > 0:
                        return [(list(init), list(rep))
                                for init, rep in sorted(outputs)]

class CoordinateNFAAddressContext(recursive.ContextBase):
    """A context for RecursiveAddress which randomly extends the starting
    coordinates in a way that is consistent with a CoordinateNFA."""
    def __init__(self, nfa, rng=random, analysis=None):
        super().__init__(nfa.tiling, nfa.base_layer.name, analysis)
        self.dfa = nfa.determinise()
        self.state = self.dfa.start
        self.layer = nfa.base_layer
        self.rng = rng

        self.probabilities = self.analysis.limiting_probabilities()

        self.proto_base_type = self.choose_symbol()
        self.proto_parents = []

    def extend_by_1(self):
        self.proto_parents.append(self.choose_symbol())

    def choose_symbol(self):
        choices = {}
        for sym, dst in self.dfa.trans[self.state]:
            tile = sym if self.state == self.dfa.start else sym[0]
            prob = self.probabilities[self.layer.name][tile]
            choices[sym, dst] = prob
        choice, nextstate = recursive.choose(self.rng, choices)

        self.layer = self.dfa.tiling.layers_by_name[self.layer.successor]
        self.state = nextstate

        return choice

    @property
    def prototype(self):
        return recursive.RecursiveAddress(
            self, self.proto_base_type, self.proto_parents)
