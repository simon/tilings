General software tool to handle substitution tilings
====================================================

This repository contains a command-line tool written in Sage, which
works with tilings of the plane generated from substitution systems,
such as Penrose tilings, or the 2023 Hat and Spectre tilings.

In particular, it does a lot of operations involving addressing tiles
in such a system by their 'combinatorial coordinates' (position in the
substitution hierarchy). Some applications of this are discussed in my
series of quasi-blog posts:

https://www.chiark.greenend.org.uk/~sgtatham/quasiblog/aperiodic-tilings/
https://www.chiark.greenend.org.uk/~sgtatham/quasiblog/aperiodic-spectre/
https://www.chiark.greenend.org.uk/~sgtatham/quasiblog/aperiodic-transducers/

Warnings
--------

This is research code: I wrote it to find out whether things would
work. So it's not production quality. In particular:

 - SECURITY: the program is written in an interpreted language, and
   both the tiling input file and some command-line options accept
   arbitrary code in that language. So don't run this tool in a
   website backend where other people can control those inputs!

 - Performance: in order to handle many tilings uniformly I use Sage's
   support for exact handling of general algebraic numbers, which is
   slow. If you want to do any of these tasks at scale (like
   generating significantly large patches of tiling), it would make
   more sense to write custom code to work in a specific algebraic
   number field, and also rewrite in a compiled language.

 - Documentation: you're looking at it, and it's not complete.

Working with existing tilings
-----------------------------

This repository already comes with some tiling descriptions, in files
with the .tl extension.

  p2-triangles.tl: Penrose P2 tiling (kites and darts), with each tile
    cut into two half-triangles, which makes it easy to work with.

  p3-triangles.tl: Penrose P3 tiling (thin and thick rhombs), again
    with each tile cut into two triangles.

  p2-whole.tl: Penrose P2 tiling with whole tiles. Some of the
    advanced functionality like --transducer fails with this version,
    but --patch generates nicer-looking output.

  p3-whole.tl: Penrose P3 tiling with whole tiles.

  hats-htpf.tl: the Hats tiling, using a non-overlapping form of the
    H,T,P,F metatile system from the paper introducing it. This system
    isn't able to generate a transducer, due to an ambiguity.

  spectre9.tl: the Spectre tiling, using the 9-hexagon metatile system
    from the paper introducing it.

  hat10.tl: the Hats tiling again, using a 10-hexagon metatile system
    derived indepently of HTPF, for which a transducer _can_ be
    generated.

Here are some sample command lines to show how to do some of the jobs.

To draw diagrams of the substitution system, use '--diagrams':

  ./tilings --diagrams spectre9.tl

  This generates a file called tiling.html and many .svg diagrams
  alongside it. To call it something else, pass --filename-prefix:

  ./tilings --diagrams spectre9.tl --filename-prefix=spectre

  This prefixes "spectre-" to all the output file names, so you get
  spectre-tiling.html as the top-level file, and all the diagrams will
  be spectre-[...].svg.

  These diagrams are useful when writing a new .tl file, and also for
  interpreting the edge numbers, child indices, and tile names in the
  input and output of other modes that deal with tiles' combinatorial
  coordinates.

To check a tiling system for consistency, use '--check':

  ./tilings --check spectre9.tl

  This is useful if you're writing a new .tl file. It prints no output
  if no errors are found; otherwise it reports a problem.

To analyse a tiling system for various properties, use '--analyse':

  ./tilings --analyse spectre9.tl

  This prints various data about the tiling. A lot of it is only
  particularly useful for debugging the internal systems that try to
  generate transducers in difficult cases. One generally useful output
  is the limiting probability distribution of the tile types in your
  tiling.

To build an adjacency recogniser for a tiling, use '--adjmatcher':

  ./tilings --adjmatcher spectre9.tl

  This dumps a full description of a state machine which takes two
  tiles' coordinate strings zipped together, augmented with a starting
  edge of each tile, and decides whether those coordinates can
  legitimately be adjacent in the tiling. State names are complicated
  Python expressions, because this is mostly useful for debugging.

To build a finite-state transducer for a tiling, use '--transducer:

  ./tilings --transducer spectre9.tl

  This dumps a full description of a state machine which takes one
  tile's coordinate string augmented with a starting edge, and
  generates output on each state transition, so that the output is the
  coordinate string of the neighbouring tile. State names are simple
  integers, so this output could be automatically turned into a lookup
  table for using the transducer in production code. However these
  can't be built for all types of tiling - not hats-htpf.tl, in
  particular.

To generate sample pairs of neighbouring tile coordinates, '--test-recurse':

  ./tilings --test-recurse spectre9.tl

  This produces pairs of finite coordinate strings that represent
  neighbouring tiles within a particular supertile. Useful for
  cross-checking with other algorithms.

  Use '--prefix' to make all the input tiles' coordinate strings start
  with a given prefix. The syntax is a Python expression in the same
  form you can see in the output, except without the edge component in
  the first element. For example, an output from the above command
  might read (with the '...' replaced with more coordinates)

  [('spectre', 0), ('Sigma', 0), ('Delta', 3), ...] -> ...

  and so a valid use of --prefix would be

  ./tilings --test-recurse spectre9.tl --prefix "[('spectre'), ('Sigma', 0), ('Delta', 3)]"

  which generates transitions in all directions from a tile with those
  low-order coordinates.

  Use '--edge' to constrain the random selection to generate
  transitions across a specific edge of the starting tile. For
  example, if you want all the output pairs to start with the symbol
  ('spectre',0) as in the above example:

  ./tilings --test-recurse spectre9.tl --prefix "[('spectre'), ('Sigma', 0), ('Delta', 3)]" --edge 0

To draw a full patch of a tiling as an SVG vector image, use '--patch':

  ./tilings --patch -o patch.svg spectre9.tl

  This generates a piece of Spectre tiling completely at random.

  To force some low-order coordinates of the starting tile but
  otherwise allow random selection, use --prefix, the same as above:

  ./tilings --patch -o patch.svg spectre9.tl --prefix "[('spectre'), ('Sigma', 0), ('Delta', 3)]"

  To specify the whole _infinite_ coordinate string of the starting
  tile in the form of an eventually periodic sequence, add --repeating
  as well as --prefix, to say what string of coordinates repeats
  forever after the prefix. This requires the tiling to be one you can
  construct a transducer for:

  ./tilings --patch -o patch.svg spectre9.tl --prefix '["spectre", ("Phi", 0)]' --repeating '[("Phi", 6)]'

  To generate a patch of the metatile layer of a tiling instead of the
  final output tiles, specify a non-default layer name from the .tl file:

  ./tilings --patch -o patch.svg spectre9.tl --base-layer hex

  By default the starting tile is placed at the origin, and scaled to
  try to generate about the same number of tiles for any tiling.
  Further tiles are generated until they fill an orthogonal rectangle
  containing the points (-20,-20) and (+20,+20); and the mathematical
  coordinates are scaled by a factor of 20 to generate the coordinates
  written into the SVG. To change those, use these options:

  ./tilings --patch -o patch.svg spectre9.tl --placement '(4,-1,1+sqrt(-1))'
  ./tilings --patch -o patch.svg spectre9.tl --bounds '(-32,18,32,-18)'
  ./tilings --patch -o patch.svg spectre9.tl --scale 2

  The --placement tuple specifies a vertex of the starting tile, and
  two complex numbers giving the endpoints of that edge. --bounds
  specifies two points in the form (x0,y0,x1,y1) that the bounding
  rectangle must contain. --scale is multiplied into the default scale
  factor of 20, so that --scale 2 makes things twice as big.

To analyse a patch of tiling obtained from somewhere else and try to
derive coordinates for it:

  ./tilings --findcoords spectre9.tl < description.txt

  The input description of a tiling patch should be written in an
  ad-hoc syntax containing two types of line:

  First it should define all the tiles in the patch, and specify each
  one's type, via lines of the form 'tile ID TYPE', where TYPE is one
  of the tile type names defined in the .tl file, and ID is an
  identifier for that particular tile. The format of ID doesn't matter
  at all (except that it can't contain spaces, because those separate
  the words of the command). You can choose those however you like.

  After that, it defines the ways the tiles connect together, via
  lines of the form 'edge ID1 EDGE1 ID2 EDGE2'. ID1 and ID2 should be
  tile identifiers previously specified in 'tile' commands; EDGE1 and
  EDGE2 are decimal integers, matching the indexing of tile edges in
  the .tl file. This specifies that the two tiles are adjacent, in
  such a way that edge EDGE1 of tile ID1 connects to edge EDGE2 of
  tile ID2.

  By default, the output of this command will try to give coordinates
  for the first tile defined in the input file. If you want the
  coordinates of a different tile, you can say '--start_tile ID' to
  specify the id of the tile you're most interested in.

  The tile coordinates will be provided in several forms. The primary
  one is a state-by-state description of a nondeterministic finite
  automaton matching _every_ possible infinite sequence of coordinates
  consistent with the tiles shown in the input. After that, the
  program will print an initial sequence of coordinates that are known
  for sure, in the sense that all paths through the NFA agree on them.
  Finally, it will search for the simplest possible
  eventually-periodic infinite sequence of coordinates consistent with
  the NFA. If multiple valid sequences are equally simple, it will
  print them all.

  If you add the '-o' option and an output file name, the tool will
  draw an SVG picture of the tile patch specified in the input. Inside
  the SVG, every tile will be annotated with the list of coordinates
  that are known for it, and also with its id.

  You can also adjust the display of that patch by adding the same
  --scale option that --patch accepts.

Writing new .tl files
---------------------

The Spectre tiling system is the best example of how to do this,
because it closely matches the way the tiling system is described in
the Spectre paper https://arxiv.org/abs/2305.17743

A tiling consists of:

 - a set of types of _edge_, each with a name and a length. Edges are
   expected to match when tiles meet. Edges can be signed, meaning
   that the +1 version must always meet the -1 version, or unsigned,
   meaning that any two instances of the edge can legally meet.
   Specify these with signed_edge(name, length) or unsigned_edge(name,
   length). You're writing Sage code, so you can specify lengths as
   algebraic numbers.

 - a set of _tile_ definitions via define_tile(name, outline). The
   outline is a sequence of edges, with turn angles between them.
   (Imagine drawing the tile via turtle graphics: you tell the turtle
   to go forward along an edge, then turn left or right by a
   particular amount, then draw another edge...) Turns are specified
   in fractions of a full turn, so left(1/4) or right(1/4) means a
   right angle, and left(1/2) or right(1/2) mean the same thing,
   namely turning round 180° and retracing your steps.

 - one or more _layer_ definitions via define_layer(name, tiles,
   successor). Each layer defines its own set of legal tiles. (E.g. in
   the Hat and Spectre tiling, there's one layer for the actual
   hats/spectres, and another for the metatiles that generate them.)
   Each layer also specifies its 'successor', the next higher-order
   layer: Spectres are expanded from their hex metatiles, and the
   hexes are expanded in turn from higher-order hexes.

 - for each pair of adjacent layers in the hierarchy, a specification
   of how each edge in the higher layer expands into a sequence of
   edges and turns in the lower layer, via expand_edge(higher_layer,
   lower_layer, edge, expansion). The expansions should be set up so
   that expanding the whole edge of a higher-layer tile gives an
   outline enclosing precisely the lower-layer tiles that it will
   generate. However outlines may also double back on themselves to
   create spurs, and sometimes have to.

   For a signed edge, you can choose whether you specify the expansion
   of its +1 or -1 variant. The other is obtained automatically by
   reversing the one you gave.

 - for each pair of adjacent layers in the hierarchy, a specification
   of what lower-layer tiles appear in the expansion of each
   upper-layer tile, via expand_tile(higher_layer, lower_layer,
   higher_tile, child_index, lower_tile, next_to, edge). This inserts
   'lower_tile' as the (child_index)th child of 'higher_tile'. 'edge'
   specifies an edge of the newly added tile, and 'next_to' says what
   existing edge it's next to. 'next_to' can be ('ext', hi, lo) to
   mean the (lo)th lower-order edge expanded from the (hi)th edge of
   the higher-order tile, or ('int', child, edge) to mean the (edge)th
   edge of the (child)th child tile (which must have been added to the
   expansion already by a previous expand_tile()).

   Child indices don't have to be numbers. They can be anything you
   can use in a Python hash. For example, in p2-triangles.tl, each
   child index is just the lowercase version of the tile type of the
   lower-order tile. (Because in that substitution system no two child
   tiles are the same type, so specifying an index isn't really
   needed. So I name the indices to make them easy to remember.)

My usual approach to defining a tiling is to do it in pretty much that
order, checking with --diagrams as I go:

 - First define the edge types, and write some define_tile() commands
   giving the outlines of the tiles. Check they look right.

 - Now define the edge expansions for a particular layer pair, and run
   --diagrams again to see what the overall outline of a tile's
   expansion looks like, and check those look right too.

 - Now you have diagrams of the individual tiles with their edges
   numbered, and diagrams of the expansion outlines with two-part
   (hi,lo) numbers on every edge. So you can begin placing child tiles
   in each expansion using define_tile, because those diagrams give
   you all the indices you need to write an 'ext' style next_to and
   say which tile edge it goes with. If you need to place tiles not in
   contact with the edge, you can use an 'int' style next_to once
   you've placed one of its neighbours. So you start at the edge of
   the expansion and work inwards.

 - Repeat the last two steps for another layer pair, if needed.

Finally, run --check to make sure everything made sense.
