# -*- python -*-

class Turtle:
    def __init__(self, pos=0, direction=1):
        self.pos = QQbar(pos)
        self.direction = QQbar(direction)
    def turn(self, v):
        self.direction *= v
    def move(self, v):
        self.pos += self.direction * v

class Polygon:
    def __init__(self, vertices, minor=set(), edgelabels={}):
        self.vertices = list(vertices)
        self.order = len(self.vertices)
        self.minor = minor
        self.edgelabels = edgelabels

    def clone(self):
        return type(self)(self.vertices, self.minor, self.edgelabels)

    def vertex(self, n):
        return self.vertices[n % self.order]

    def transform(self, offset, scale):
        for i, v in enumerate(self.vertices):
            self.vertices[i] = offset + v * scale

    def place_next_to_points(self, edge, v0, v1):
        p0 = self.vertex(edge)
        p1 = self.vertex(edge + 1)
        scale = (v1 - v0) / (p1 - p0)
        offset = v0 - scale * p0
        self.transform(offset, scale)

    def place_next_to_edge(self, self_edge, rhs, rhs_edge, reverse=False):
        v0 = rhs.vertex(rhs_edge+1)
        v1 = rhs.vertex(rhs_edge)
        if reverse:
            v1, v0 = v0, v1
        self.place_next_to_points(self_edge, v0, v1)

    def iter_edges(self):
        for i in range(self.order):
            yield self.vertex(i), self.vertex(i+1)

    def labelpos(self):
        # FIXME: we can probably do better than this, by a similar
        # technique to find_incentre() in Puzzles grid.c
        return sum(self.vertices) / self.order

    def draw(self, svg, edgelabel_sign=0, blobs=True, big_blobs=False,
             cssclass=None, label=None, arrow=None, data=None):
        def edgelabel_text(i):
            numbers = self.edgelabels.get(i, ())
            return ".".join(map(str, reversed(numbers)))
        if label is not None:
            labelpos = self.labelpos()
        else:
            labelpos = None
        svg.add_polygon(self.vertices, edgelabel_sign=edgelabel_sign,
                        edgelabel_text=edgelabel_text, cssclass=cssclass,
                        labelpos=labelpos, label=label, data=data)
        if arrow is not None:
            start, end = arrow(self.vertices)
            svg.add_arrow(start, end)
        if blobs:
            for i, v in enumerate(self.vertices):
                svg.add_vertex_blob(v, big_blobs and i not in self.minor)

    def area(self):
        # Calculate the area of this polygon, using the shoelace formula.
        return abs(sum(self.vertex(j).real() *
                       (self.vertex(j-1) - self.vertex(j+1)).imag()
                       for j in range(len(self.vertices))) / 2)
    def __hash__(self):
        return hash((self.vertices[0], self.vertices[1]))
    def __eq__(self, rhs):
        return self.vertices == rhs.vertices
    def __ne__(self, rhs):
        return self.vertices != rhs.vertices

# Thrown in recursive.sage if we aren't allowed to go off the top of a
# finite coordinate string
class InextensibleAddressException(Exception):
    pass

# Thrown in transducer.sage if a FakeTransducer fails to find a unique
# neighbour of an infinite coordinate string
class InfiniteAmbiguityException(Exception):
    pass
