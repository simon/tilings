# -*- python -*-

import hashlib
import random

import analysis
import util

class DeterministicRandomGenerator:
    def __init__(self, seed = b'nonsense wonsense'):
        self.index = 0
        self.seed = seed

    def _upto(self, limit):
        hashval = int(hashlib.sha256(
            self.seed + str(self.index).encode('ASCII')).hexdigest(), 16)
        self.index += 1
        return (hashval * limit) >> 256

    def randint(self, lo, hi_inclusive):
        return lo + self._upto(hi_inclusive + 1 - lo)

    def uniform(self, base, limit):
        # BODGE: it'd be nicer to do this rigorously by taking a bit
        # at a time!
        return base + self._upto(2**256) / 2**256

class ContextBase:
    # Work around the fact that I want one of __init__'s keyword parameters
    # to be called 'analysis' which shadows the module import above
    def _real_init(self, tiling, base_layer_name, an):
        self.tiling = tiling
        self.analysis = (an if an is not None else
                         analysis.Analysis(self.tiling))
        self.base_layer = (
            self.tiling.layers_by_name[base_layer_name]
            if base_layer_name is not None
            else self.tiling.layers[0])

    def __init__(self, tiling, base_layer_name=None, analysis=None):
        self._real_init(tiling, base_layer_name, analysis)

def choose(rng, probabilities):
    # probabilities[] is a dict mapping a list of choices to their
    # probabilities. The probabilities are allowed to sum to a value
    # other than 1; they'll be normalised here.

    choices = sorted(probabilities.items())
    total_prob = sum(p for _,p in choices)

    randval = rng.uniform(0, 1)
    cumulative = 0

    for choice, prob in choices:
        cumulative += prob / total_prob
        if randval <= cumulative:
            return choice

    assert False, f"random selection got confused: {choices!r}"

class InextensibleAddressContext(ContextBase):
    # Simplest possible implementation of the address context API:
    # when asked to extend a tile address to add more parents, refuse,
    # and throw an exception.
    def __init__(self, tiling, base_layer_name=None, analysis=None):
        super().__init__(tiling, base_layer_name, analysis)
        self.proto_parents = []

    def extend_by_1(self):
        raise util.InextensibleAddressException()

class RandomAddressContext(ContextBase):
    def __init__(self, tiling, base_type, parents,
                 rng=random, base_layer_name=None, analysis=None):
        super().__init__(tiling, base_layer_name, analysis)

        self.rng = rng

        self.probabilities = self.analysis.limiting_probabilities()

        if base_type is None:
            base_type = choose(self.rng, {
                tile: self.probabilities[self.base_layer.name][tile]
                for tile in self.base_layer.tiles
            })

        self.proto_base_type = base_type
        self.curr_layer = self.base_layer
        assert base_type in self.curr_layer.tiles, (
            f"Bad base type {base_type}: should be in {self.curr_layer.tiles}")
        for parent_type, _ in parents:
            self.curr_layer = self.tiling.layers_by_name[
                self.curr_layer.successor]
            assert parent_type in self.curr_layer.tiles, (
                f"Parent type {parent_type} not in "
                f"{self.curr_layer.name} layer")
        self.proto_parents = parents
        self.next_layer = self.tiling.layers_by_name[self.curr_layer.successor]

    def extend_by_1(self):
        last_tile_type = (
            self.proto_parents[-1][0] if len(self.proto_parents) > 0
            else self.proto_base_type)

        expansion = self.tiling.expansions[
            self.next_layer.name, self.curr_layer.name]
        self.proto_parents.append(choose(self.rng, {
            (tile, index): self.probabilities[self.next_layer.name][tile]
            for tile, index in expansion.permitted_parents[last_tile_type]
        }))

        self.curr_layer = self.next_layer
        self.next_layer = self.tiling.layers_by_name[self.next_layer.successor]

    @property
    def prototype(self):
        return RecursiveAddress(self, self.proto_base_type, self.proto_parents)

class RecursiveAddress:
    def __init__(self, context, base_type, parents):
        self.context = context
        self.types = [base_type] + [p[0] for p in parents]
        self.indices = [p[1] for p in parents]

    def _extend(self):
        extra = self.context.proto_parents[len(self.indices):]
        self.types.extend(p[0] for p in extra)
        self.indices.extend(p[1] for p in extra)

    def _ensure(self, n):
        while len(self.types) <= n:
            self._extend()
            if len(self.types) <= n:
                self.context.extend_by_1()

    def clone(self):
        return type(self)(self.context, self.types[0],
                          list(zip(self.types[1:], self.indices)))

    def _recurse(self, depth, childlayer, input_edge):
        self._ensure(depth+1)

        parentlayer = self.context.tiling.layers_by_name[childlayer.successor]
        expansion = self.context.tiling.expansions[
            parentlayer.name, childlayer.name]

        parent_tile = self.types[depth+1]
        child_index = self.indices[depth]

        nt = self.context.analysis.neighbour_table(expansion, parent_tile)
        kind, hi, lo = nt['int', child_index, input_edge]

        while kind == 'ext':
            parent_edge, out_subedge = hi, lo
            edge_type = self.context.tiling.tile_edges[parent_tile][parent_edge]
            expanded_edge_len = len(expansion.edge_edges[edge_type])
            in_subedge = expanded_edge_len - 1 - out_subedge

            in_parent_edge = self._recurse(depth+1, parentlayer, parent_edge)

            parent_tile = self.types[depth+1]
            nt = self.context.analysis.neighbour_table(expansion, parent_tile)
            kind, hi, lo = nt['ext', in_parent_edge, in_subedge]

        self.indices[depth] = hi
        self.types[depth] = expansion.tiles[parent_tile][hi][0]
        return lo

    def step(self, edge, infinite_boundary=lambda:None):
        return self._recurse(0, self.context.base_layer, edge)

    @property
    def base_type(self):
        return self.types[0]

    def coords(self, edge=None):
        init = self.types[0]
        if edge is not None:
            init = (init, edge)
        return [init] + list(zip(self.types[1:], self.indices))

    @property
    def first_parent(self):
        return (self.types[1], self.indices[0])

    def __str__(self):
        self._extend()
        return " ".join(map(repr, self.coords()))
