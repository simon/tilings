# Wrapper to allow 'import' to be used with a .sage file
from sage.all_cmdline import *
assert __file__.endswith(".py")
load(__file__[:-3] + ".sage")
