# -*- python -*-

'''
Analysis and generation tool for substitution tilings.
'''

import argparse
import collections
import itertools
import html
import random
import sys
import timeit

from svg import SVG
import analysis
import draw
import finder
import infinitary
import recursive
import tilingdef
import transducer
import util

def diagrams(args, tiling):
    def make_svg(size, expansion=None):
        svg = SVG(size, fontscale=args.fontscale, edgelabelscale=args.fontscale)
        for tile, colour in tiling.tile_colours.items():
            svg.add_css(f'.tile-{tile} {{ fill: {colour}; }}\n')
        if expansion is not None:
            for tile, child_colours in expansion.child_colours.items():
                for index, colour in child_colours.items():
                    svg.add_css(f'.tile-{tile}-{index} {{ fill: {colour}; }}\n')
        return svg

    title = html.escape(f"Tile and expansion diagrams")
    page = f"<html>\n<head><title>{title}</title></head>\n<body><h1>{title}</h1>"

    prefix = ("" if args.filename_prefix is None
              else args.filename_prefix + "-")

    for tile in tiling.tiles:
        outline, orientation = tiling.tile_outline(tile)
        polygon = tiling.polygon_from_outline(outline, orientation)

        svg = make_svg(60 * args.scale)
        # FIXME: label edges somehow
        label = str(tile) if args.tile_labels else None
        polygon.draw(svg, edgelabel_sign=-1, cssclass=f'tile-{tile}',
                     label=label, arrow=tiling.tile_arrows.get(tile))
        fname = f'{prefix}tile-{tile}.svg'
        with open(fname, 'w') as fh:
            # In the tile diagrams, all the edge labels are on the
            # inside, so we don't need to worry about expanding the
            # SVG bounds to account for text outside the tile
            svg.write(fh, expand_for_exterior_text=False)
        qtile = html.escape(tile)
        qfname = html.escape(fname)
        page += f'<p>Tile diagram for {qtile}:<br><img src="{qfname}"></p>\n'

    for expansion in tiling.expansions.values():
        for tile in tiling.tiles:
            if tile not in expansion.parentlayer.tiles:
                continue
            outline_polygon, child_polygons, orientation = (
                expansion.expand_tile(tile))

            svg = make_svg(30 * args.scale, expansion)
            outline_polygon.draw(
                svg, big_blobs=True,
                edgelabel_sign=-1 if expansion.is_reversing else +1)
            for child_index, child_polygon in child_polygons.items():
                child_tile = expansion.tiles[tile][child_index][0]
                cssclass = f'tile-{child_tile}'
                if child_index in expansion.child_colours[tile]:
                    cssclass = f'tile-{tile}-{child_index}'
                label = str(child_index)
                if args.tile_labels:
                    label += f" ({child_tile})"
                child_polygon.draw(svg, edgelabel_sign=-1,
                                   cssclass=cssclass, label=label,
                                   arrow=tiling.tile_arrows.get(child_tile))
            fname = (f'{prefix}expand-{tile}-{expansion.parentlayer.name}-to-'
                     f'{expansion.childlayer.name}.svg')
            with open(fname, 'w') as fh:
                svg.write(fh)
            qtile = html.escape(tile)
            qparent = html.escape(expansion.parentlayer.name)
            qchild = html.escape(expansion.childlayer.name)
            qfname = html.escape(fname)
            page += (f'<p>Expansion of {qtile} from {qparent} to {qchild}:'
                     f'<br><img src="{qfname}"></p>\n')

    page += "</body></html>\n"
    with open(f"{prefix}tiling.html", 'w') as fh:
        fh.write(page)

def check_validity(tiling):
    ok = True
    def error(*args):
        nonlocal ok
        ok = False
        print(*args)

    # Check each tile closes up
    for tile in tiling.tiles:
        outline, orientation = tiling.tile_outline(tile)
        turtle = util.Turtle()
        tiling.polygon_from_outline(outline, turtle=turtle)
        if turtle.pos != 0:
            error(f'{tile} does not return to its origin')
        if turtle.direction != 1:
            error(f'{tile} does not return to its starting orientation')

    # Check each edge expansion doesn't change the orientation
    for expansion in tiling.expansions.values():
        for (edge, sign), outline in expansion.edges.items():
            angle = 1
            for element in outline:
                if element[0] == 'turn':
                    angle *= element[1]

            if angle != 1:
                error(f'Expansion of edge ({edge}, {sign}) from '
                      f'{expansion.parentlayer.name} to '
                      f'{expansion.childlayer.name}: total angle is nonzero')

    # Check adjacencies in tile expansions
    for expansion in tiling.expansions.values():
        for tile in expansion.parentlayer.tiles:
            outline, orientation = tiling.tile_outline(tile)
            outline = expansion.expand_outline(outline)
            turtle = util.Turtle()
            tiling.polygon_from_outline(outline, turtle=turtle)
            if turtle.pos != 0:
                error(f'Expansion of {tile} from '
                      f'{expansion.parentlayer.name} to '
                      f'{expansion.childlayer.name} '
                      f'does not return to its origin')
            if turtle.direction != 1:
                error(f'Expansion of {tile} from '
                      f'{expansion.parentlayer.name} to '
                      f'{expansion.childlayer.name} '
                      f'does not return to its starting orientation')

            # Check the edge dictionary makes sense
            edgedict = analysis.build_edge_dict(expansion, tile)
            edgedict_bad = False
            for (v1, v2), edges in edgedict.items():
                edges2 = edgedict.get((v2, v1))
                if edges2 is None:
                    error(f'Expansion of {tile} from '
                          f'{expansion.parentlayer.name} to '
                          f'{expansion.childlayer.name}: edge '
                          f'{edges[0]} has nothing on the other side')
                    edgedict_bad = True
                elif len(edges) != len(edges2):
                    error(f'Expansion of {tile} from '
                          f'{expansion.parentlayer.name} to '
                          f'{expansion.childlayer.name}: edges '
                          f'{",".join(map(str, edges))} and '
                          f'{",".join(map(str, edges))} join to each other '
                          f'but there are different numbers of them')
                    edgedict_bad = True

            if edgedict_bad:
                continue

            neighbours = analysis.build_neighbour_table(edgedict)

            # Verify that edge types match
            def edgetype(key):
                kind, hi, lo = key
                if kind == 'ext':
                    # Find the edge type of the parent tile
                    parent_edge_type = tiling.tile_edges[tile][hi]
                    # Find the sub-edge within that edge expansion
                    edgetype = expansion.edge_edges[parent_edge_type][lo]
                    # And reverse it, because we're on the other side of it
                    return edgetype[0], -edgetype[1]
                elif kind == 'int':
                    # Find the child tile type
                    child_tile = expansion.tiles[tile][hi][0]
                    # And find the type of the appropriate edge of it
                    return tiling.tile_edges[child_tile][lo]
            for key1, key2 in neighbours.items():
                e1 = edgetype(key1)
                e2 = edgetype(key2)
                if (e1[0], e1[1]) != (e2[0], -e2[1]):
                    error(f'Expansion of {tile} from '
                          f'{expansion.parentlayer.name} to '
                          f'{expansion.childlayer.name}: adjacent edges '
                          f'{key1}, {key2} have mismatched edge types '
                          f'{e1}, {e2}')

    return ok

def check(args, tiling):
    check_validity(tiling)

def analyse(args, tiling):
    an = analysis.Analysis(tiling)

    for expansion in tiling.expansions.values():
        print("Permitted (parent, index) pairs in expansion from "
              f"{expansion.parentlayer.name} to {expansion.childlayer.name}:")
        for child, parents in sorted(expansion.permitted_parents.items()):
            pstr = ",".join(f"{p} #{i}" for p,i in sorted(parents))
            print(f"  {child}: {pstr}")

    print("Limiting probability distributions:")
    for layer, probs in sorted(an.limiting_probabilities().items()):
        print(f"  {layer}:")
        for tile, prob in sorted(probs.items()):
            print(f"    {tile}: {prob}")

    mte = an.meeting_tile_edges()
    print("Meeting tile edges:")
    for (t1, i1), edges2 in sorted(mte.items()):
        print(f"  {t1} edge #{i1} ->", ", ".join(
            f"{t2} edge #{i2}" for t2, i2 in edges2
        ))

    mas = an.meetings_across_spurs()
    print("Meetings across spurs:")
    for (expansion, t1, t2, hi1, lo1, hi2, lo2), pairs in sorted(mas.items()):
        for i1, i2 in sorted(pairs):
            print(f"  {expansion.parentlayer.name} -> "
                  f"{expansion.childlayer.name}: "
                  f"{t1} edge {hi1}.{lo1} meets {t2} edge {hi2}.{lo2} when "
                  f"{t1} edge {i1} meets {t2} edge {i2}")

def build_adjmatcher(args, tiling):
    am = transducer.AdjacencyMatcher(tiling)
    am.generate(verbose=args.verbose)
    for src, transitions in sorted(am.transitions.items()):
        print(f"State {src}:")
        print(f"  # {am.state_descs[src]}")
        print(f"  # path from start:",
              " ".join(map(str, am.paths_from_start[src])))
        print(f"  # path to accept:",
              " ".join(map(str, am.paths_to_accept[src])))
        for sym, dest in transitions:
            print(f"  {sym} -> {dest}")
    missing = am.find_unaccepted_inputs()
    try:
        first_missing = next(missing)
        print(f"Adjacency matcher does not accept legal inputs:")
        print(first_missing)
        for next_missing in missing:
            print(next_missing)
    except StopIteration:
        print("Adjacency matcher accepts all legal inputs")

def build_transducer(args, tiling):
    am = transducer.AdjacencyMatcher(tiling)
    am.generate()

    tr = transducer.Transducer(am)
    tr.build()

    if args.graphviz:
        tr.emit_graphviz(sys.stdout)
        return

    for src, transitions in sorted(tr.trans.items()):
        print(f"State {src}" + (" (accepting)" if src in tr.accept else "") +
              ":")
        for sym, (dest, output) in transitions.items():
            print(f"  {sym}: output {output}, go to {dest}")

    missing = tr.find_unaccepted_input()
    if missing is None:
        print("Transducer accepts all legal inputs")
    else:
        print(f"Transducer does not accept this legal input:\n{missing}")

def gen_recursive_tests(tiling, n, prefix=[], edge=None):
    an = analysis.Analysis(tiling)
    rng = recursive.DeterministicRandomGenerator()
    seen = set()
    base_type = None if len(prefix) == 0 else prefix[0]
    parents = prefix[1:]
    while len(seen) < n:
        ctx = recursive.RandomAddressContext(
            tiling, base_type, parents.copy(), rng, analysis=an)

        iaddr = ctx.prototype
        iedge = (edge if edge is not None else
                 rng.randint(0, len(tiling.tile_edges[iaddr.base_type])-1))

        oaddr = ctx.prototype.clone()
        oedge = oaddr.step(iedge)

        iaddr._extend()
        oaddr._extend()

        key = tuple(iaddr.coords(iedge))
        if key not in seen:
            seen.add(key)
            yield iaddr.coords(iedge), oaddr.coords(oedge)

def test_recurse(args, tiling):
    for icoords, ocoords in gen_recursive_tests(
            tiling, 1000, args.prefix, args.edge):
        print(icoords, "->", ocoords)

def draw_patch(args, tiling):
    if args.output is None:
        sys.exit("Must specify the -o output filename option in --patch mode")
    # Normalise bounds to min/max
    bounds = (
        min(args.bounds[0], args.bounds[2]), # xmin
        min(args.bounds[1], args.bounds[3]), # ymin
        max(args.bounds[0], args.bounds[2]), # xmax
        max(args.bounds[1], args.bounds[3]), # ymax
    )
    base_layer = (tiling.layers[0] if args.base_layer is None
                  else tiling.layers_by_name[args.base_layer])
    target_area = draw.Rectangle(*bounds)
    diagram = draw.Diagram(tiling, base_layer.name, target_area)

    if args.repeating:
        am = transducer.AdjacencyMatcher(tiling, base_layer=base_layer)
        am.generate()
        if not args.use_adjmatcher:
            tr = transducer.Transducer(am)
            tr.build()
        else:
            tr = transducer.FakeTransducer(am)
        coords = infinitary.EventuallyPeriodicAddress(
            tr, args.prefix, args.repeating)
    else:
        if args.prefix:
            base_type, parents = args.prefix[0], args.prefix[1:]
        else:
            base_type, parents = None, []
        if args.inextensible:
            context = recursive.InextensibleAddressContext(
                tiling, base_layer_name=base_layer.name)
            coords = recursive.RecursiveAddress(context, base_type, parents)
        else:
            context = recursive.RandomAddressContext(
                tiling, base_type, parents, base_layer_name=base_layer.name)
            if len(context.proto_parents) < 1:
                context.extend_by_1() # ensure we can colour the base tile
            coords = context.prototype

    poly = tiling.tile_polygon(coords.base_type)
    if args.placement is not None:
        placement = args.placement
    else:
        # Work out the average area of a tile, using the limiting
        # probability distribution of tiles.
        an = analysis.Analysis(tiling)
        tile_probs = an.limiting_probabilities()[base_layer.name]
        mean_area = sum(
            prob * tiling.tile_polygon(tile).area()
            for tile, prob in tile_probs.items())

        # Find a scale factor that normalises that to within 5% of an
        # arbitrary target area without introducing a ludicrous amount
        # of extra algebraic computation, by finding a rational
        # convergent of the square root of the ideal scale factor.
        #
        # This arranges that when you just pick a tiling and say 'go',
        # you get a roughly comparable number of tiles in the output
        # no matter what tiling it was.
        target_area = 5/2
        convs = iter(continued_fraction(
            sqrt(target_area/mean_area)).convergents())
        scale_q = next(convs)
        for conv in convs:
            prevconv, scale_q = scale_q, conv
            if 19/20 < scale_q / conv < 20/10:
                break

        # Scale the starting tile to that size, and centre it
        # (roughly, via the mean of its vertices) on the origin.
        centre = sum(poly.vertices) / len(poly.vertices)
        placement = (0, (poly.vertex(0)-centre)*scale_q,
                     (poly.vertex(1)-centre)*scale_q)
    poly.place_next_to_points(*placement)
    diagram.add_tile(poly, coords)
    diagram.generate()

    css_classes_generated = set()
    def add_css_class(cssclass, colour):
        if cssclass in css_classes_generated:
            return
        svg.add_css(f'.{cssclass} {{ fill: {colour}; }}\n')
        css_classes_generated.add(cssclass)

    if args.format == 'svg':
        scale = 20 * args.scale
        svg = SVG(scale)
        for polygon, coords in diagram.geom_to_comb.items():
            cssclass = None
            if coords.base_type in tiling.tile_colours:
                cssclass = "tile-" + coords.base_type
                add_css_class(cssclass, tiling.tile_colours[coords.base_type])
            else:
                try:
                    parent, index = coords.first_parent
                    parentdict = diagram.expansion.child_colours[parent]
                    if index in parentdict:
                        cssclass = f"tile-{parent}-{index}"
                        add_css_class(cssclass, parentdict[index])
                except IndexError:
                    pass # run out of ways to try to colour this tile
            polygon.draw(svg, blobs=False, cssclass=cssclass,
                         arrow=tiling.tile_arrows.get(coords.base_type),
                         data={'coords': str(coords)})

        if args.boundary:
            svg.add_css("path.boundary { stroke-width: 5; }\n")
            for path in diagram.boundary_paths():
                svg.add_path(path, cssclass="boundary")

        svg.force_bounds(*bounds)
        with open(args.output, "w") as fh:
            svg.write(fh)
    elif args.format == 'findcoords':
        polyindex = {}
        edges = {}
        adj = collections.defaultdict(dict) # tile1 -> tile2 -> (edge1, edge2)
        indexgen = (f"T{j}" for j in itertools.count())

        with open(args.output, "w") as fh:
            for polygon, coords in diagram.geom_to_comb.items():
                index = next(indexgen)
                polyindex[polygon] = index
                print("tile", index, coords.base_type, file=fh)
                for ei, edge in enumerate(polygon.iter_edges()):
                    edges[edge] = index, ei

            for (v, w), (pi, ei) in edges.items():
                try:
                    pi2, ei2 = edges[w, v]
                    adj[pi][pi2] = (ei, ei2)
                    adj[pi2][pi] = (ei2, ei)
                except KeyError:
                    pass

            for pi, neighbours in adj.items():
                for pi2, (ei, ei2) in neighbours.items():
                    print("edge", pi, ei, pi2, ei2, file=fh)

def find_coords(args, tiling):
    # Type of each tile.
    types = {}

    # Adjacency graph of the tiles, with each edge annotated by the
    # indices of a pair of tile edges that meet. We only need one
    # index per pair of adjacent tiles.
    adj = collections.defaultdict(dict) # tile1 -> tile2 -> (edge1, edge2)

    start_tile = None # FIXME: configure this
    fh = sys.stdin # FIXME: input file name?

    for line in iter(fh.readline, ''):
        words = line.rstrip("\r\n").split(" ")
        if len(words) == 0 or len(words[0]) == 0 or words[0].startswith("#"):
            continue # tolerate blank lines and comments
        if words[0] == 'tile':
            types[words[1]] = words[2]
            if start_tile is None:
                start_tile = words[1] # default start at first tile in file
        elif words[0] == 'edge':
            adj[words[1]][words[3]] = (int(words[2]), int(words[4]))
            adj[words[3]][words[1]] = (int(words[4]), int(words[2]))
        else:
            assert False, f"bad graph keyword {words[0]}"

    # Depth-first search to find a walk visiting all tiles.
    def dfs_visit(v, visited=set()):
        visited.add(v)
        yield 'yield', v
        for w in adj[v]:
            if w not in visited:
                yield 'call', dfs_visit(w, visited)
                yield 'yield', v
        yield ('return',)
    def executor(gen):
        stack = [gen]
        while len(stack) > 0:
            gen = stack[-1]
            action = next(gen)
            if action[0] == 'call':
                stack.append(action[1])
            elif action[0] == 'return':
                stack.pop()
            elif action[0] == 'yield':
                yield action[1]
            else:
                assert False, f"??? {action[0]}"
    walk = list(executor(dfs_visit(start_tile)))

    base_layer = (tiling.layers[0] if args.base_layer is None
                  else tiling.layers_by_name[args.base_layer])
    am = transducer.AdjacencyMatcher(tiling, base_layer=base_layer)
    am.generate()

    c = finder.CoordinateNFA.valid_for_tile(tiling, types[walk[0]],
                                            base_layer=base_layer)
    for j, (v, w) in enumerate(itertools.pairwise(walk)):
        ve, we = adj[v][w]
        vt, wt = types[v], types[w]
        if args.verbose:
            print(f"Coords at tile {v} (type {vt}):")
            c.dump()
            print(f"Step from {v} ({vt}) edge {ve} -> {w} ({wt}) edge {we}")
        c = c.valid_neighbour(am, ve, wt, we)
        if c.empty():
            print("No valid coordinates")
            return
    c = c.determinise()
    c.dump()
    print("Known:", c.known_coords())
    print("Simplest repeating:")
    for init, rep in c.simplest_rho():
        print(f"  {init}, {rep}")

    if args.output is not None:
        diagram = draw.Diagram(tiling, base_layer.name,
                               draw.UnboundedTargetArea())
        startpoly = poly = tiling.tile_polygon(types[start_tile])
        ids = {}
        for j, (v, w) in enumerate(itertools.pairwise(walk)):
            ids[poly] = v
            diagram.add_tile(poly, c.known_coords())
            ve, we = adj[v][w]
            vt, wt = types[v], types[w]
            c = c.valid_neighbour(am, ve, wt, we)
            oldpoly, poly = poly, tiling.tile_polygon(wt)
            poly.place_next_to_edge(we, oldpoly, ve)

        scale = 20 * args.scale
        svg = SVG(scale)
        svg.add_css('.start { fill: red; }\n.tile { fill: #808080; }\n')
        for poly, coords in diagram.geom_to_comb.items():
            cssclass = "start" if poly == startpoly else "tile"
            poly.draw(svg, blobs=False, cssclass=cssclass,
                      arrow=tiling.tile_arrows.get(coords[0]),
                      data={'known': str(coords), 'id': ids[poly]})
        with open(args.output, "w") as fh:
            svg.write(fh)

def selftest():
    CheckOnly = object() # the tiling validates, but can't build an adjmatcher
    AdjMatcher = object() # built adjmatcher but not deterministic transducer
    Success = object() # everything works

    working_files = [
        ('p2-triangles.tl', Success),
        ('p3-triangles.tl', Success),
        ('p2-whole.tl', Success),
        ('p3-whole.tl', Success),
        ('spectre9.tl', Success),
        ('tridrafter.tl', Success),
        ('hats-htpf.tl', AdjMatcher),
        ('hat10.tl', Success),
        ('hats-hhtpfff.tl', Success),
        ('hats-hhtpfff-nicer.tl', Success),
        ('chair.tl', AdjMatcher),
        ('ammann-beenker.tl', AdjMatcher),
        ('spectre-h7h8.tl', AdjMatcher),
        ('spectre-h7h8b.tl', AdjMatcher),
        ('hats-h7h8.tl', AdjMatcher),
        ('hats-h7h8b.tl', AdjMatcher),
        ('spectre-h7h8-9.tl', Success),
        ('hats-h7h8-10.tl', Success),
    ]
    for filename, level in working_files:
        print(f"{filename}:")
        with open(filename) as f:
            tiling = tilingdef.load_tiling(f)

        assert check_validity(tiling)
        print(f"  validity checks passed")

        if level == CheckOnly:
            continue

        am = transducer.AdjacencyMatcher(tiling)
        seconds = timeit.timeit(am.generate, number=1)

        assert am.find_unaccepted_input() is None
        assert all(s in am.paths_from_start
                   for s in range(len(am.state_descs)))
        assert all(s in am.paths_to_accept
                   for s in range(len(am.state_descs)))
        print(f"  adjacency matcher built ok: "
              f"{len(am.state_descs)} states, {seconds:.2f} s")

        if level == Success:
            tr = transducer.Transducer(am)
            seconds = timeit.timeit(tr.build, number=1)

            assert tr.find_unaccepted_input() is None
            print(f"  transducer built ok: "
                  f"{len(tr.desc)} states, {seconds:.2f} s")

            for icoords, ocoords_r in gen_recursive_tests(tiling, 1000):
                ocoords_t = tr.map_finite_string(icoords)
                assert list(ocoords_r) == list(ocoords_t), (
                    f"input {icoords!r}: recursive gives {ocoords_r!r}, "
                    f"transducer gives {ocoords_t!r}")
            print(f"  transducer matches recursive algorithm")

        else:
            # If we can't build a transducer, we can at least check
            # that the adjacency matcher can handle (input, output)
            # pairs from the recursive algorithm.
            for icoords, ocoords in gen_recursive_tests(tiling, 1000):
                assert am.check_string(zip(icoords, ocoords)), (
                    f"input {icoords!r}: recursive gives {ocoords!r}, "
                    "adjacency matcher rejects the combination")
            print(f"  adjacency matcher accepts recursive algorithm's output")

class SelftestAction(argparse.Action):
    # An option-parsing class that will make --selftest go straight to
    # the test routine without complaining that I didn't specify a
    # tiling file name.
    def __call__(self, parser, *args, **kws):
        selftest()
        parser.exit()

def main():
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter)

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "--diagrams", help="Draw diagrams of the tiling system.",
        action="store_const", dest="action", const=diagrams)
    group.add_argument(
        "--check", help="Check consistency of the tiling system.",
        action="store_const", dest="action", const=check)
    group.add_argument(
        "--analyse", help="Print analyses of the tiling system.",
        action="store_const", dest="action", const=analyse)
    group.add_argument(
        "--selftest", help="Test operations that ought to work.",
        nargs=0, action=SelftestAction)
    group.add_argument(
        "--adjmatcher", help="Build an adjacency-recognising DFA for the "
        "tiling.", action="store_const", dest="action", const=build_adjmatcher)
    group.add_argument(
        "--transducer", help="Try to build a transducer for the tiling.",
        action="store_const", dest="action", const=build_transducer)
    group.add_argument(
        "--test-recurse", help="Generate some test transitions using the "
        "recursive algorithm.",
        action="store_const", dest="action", const=test_recurse)
    group.add_argument(
        "--patch", help="Generate a patch of tiling and output an SVG.",
        action="store_const", dest="action", const=draw_patch)
    group.add_argument(
        "--findcoords", help="Read a graph description of a tiling and "
        "determine the coordinates of a tile.",
        action="store_const", dest="action", const=find_coords)
    parser.add_argument("tiling", help="Tiling definition file.")
    parser.add_argument(
        "--param", nargs=2, metavar=('name', 'value'), action='append',
        help="Parameter for the tiling file to access via parameter().")
    parser.add_argument(
        "--prefix", type=eval, default=[],
        help="Prefix to prepend to coordinates in --test-recurse mode, "
        "or to use as the starting coordinates in --patch mode.")
    parser.add_argument(
        "--repeating", type=eval, default=[],
        help="String of coordinates to repeat endlessly in --patch mode, "
        "using a transducer to generate the tiling.")
    parser.add_argument(
        "--edge", type=int,
        help="Which edge to try to traverse in --test-recurse mode")
    parser.add_argument(
        "--graphviz", action="store_true",
        help="Emit state machines in Graphviz format, in --adjmatcher and "
        "--transducer modes")
    parser.add_argument(
        "--filename-prefix", "--filename_prefix",
        help="Prefix to prepend to filenames in --diagrams mode")
    parser.add_argument(
        "--scale", type=float, default=1,
        help="Scale diagrams by this factor in --diagrams or --patch mode")
    parser.add_argument(
        "--tile-labels", "--tile_labels", action="store_true",
        help="In --diagrams mode, label each tile with its type.")
    parser.add_argument(
        "--fontscale", type=float, default=1,
        help="Scale text by this factor in --diagrams mode")
    parser.add_argument(
        "--base-layer", "--base_layer",
        help="Layer of tiling to start at in --patch mode")
    parser.add_argument(
        "--bounds", type=eval, default=(-20, -20, +20, +20),
        help="Bounding rectangle for --patch mode, in the form (x0, y0, x1, y1)")
    parser.add_argument(
        "--placement", type=lambda s: eval(s, globals()),
        help="Placement of the starting tile for --patch mode, in the form (vertex index, start point, end point), with the points being complex numbers")
    parser.add_argument(
        "--boundary", action="store_true",
        help="Show infinite supertile boundaries, in --patch mode, when using "
        "infinitary coordinates.")
    parser.add_argument(
        "--inextensible", action="store_true",
        help="Don't extend the starting coordinate prefix, in --patch mode.")
    parser.add_argument(
        "--start_tile", help="Tile to determine coordinates of in --findcoords mode")
    parser.add_argument(
        "--use-adjmatcher", "--use_adjmatcher", action="store_true",
        help="In --patch mode, don't try to build and use a transducer. "
        "Instead just use the adjacency recogniser and abort on ambiguity.")
    parser.add_argument(
        "-o", "--output",
        help="Output file name for --patch, or to write an annotated SVG "
        "to in --findcoords.")
    parser.add_argument(
        "--format", choices=['svg', 'findcoords'], default='svg',
        help="Output format for --patch: SVG, or --findcoords input.")
    parser.add_argument(
        "-v", "--verbose", action="store_true",
        help="Output diagnostics, for --findcoords and --adjmatcher")

    args = parser.parse_args()

    params = dict(args.param) if args.param is not None else {}

    with open(args.tiling) as f:
        tiling = tilingdef.load_tiling(f, params)

    args.action(args, tiling)

if __name__ == '__main__':
    main()
