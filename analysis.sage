# -*- python -*-

import collections
import itertools

def build_edge_dict(expansion, tile):
    outline_polygon, child_polygons, orientation = expansion.expand_tile(tile)

    """Return a dictionary mapping ordered vertex pairs in the plane to a
    list of things on that side of the edge between them, for a given
    tile expansion in a tiling.

    In a well-specified tiling all lists should have size 1, but this
    function leaves it to the caller to check that.
    """

    edgedict = collections.defaultdict(list)
    for i, (v1, v2) in enumerate(outline_polygon.iter_edges()):
        if expansion.is_reversing:
            v1, v2 = v2, v1
        edgedict[v2, v1].append(('ext',) + outline_polygon.edgelabels[i])
    for child_index, child_polygon in child_polygons.items():
        for i, (v1, v2) in enumerate(child_polygon.iter_edges()):
            edgedict[v1, v2].append(('int', child_index, i))
    return edgedict

def build_neighbour_table(edgedict):
    """Return a dictionary mapping each side of each edge to the other,
    derived from the result of build_edge_dict()."""
    table = {}
    for (v1, v2), maps12 in edgedict.items():
        maps21 = edgedict[v2, v1]
        for key12, key21 in zip(sorted(maps12), sorted(maps21)):
            table[key12] = key21
    return table

class Analysis:
    def __init__(self, tiling):
        self.tiling = tiling
        self.edge_dicts = {} # (expansion, tile) -> edgedict
        self.neighbour_tables = {} # (expansion, tile) -> table
        self.mte = self.mas = self.parents = self.probs = None

    def edge_dict(self, expansion, tile):
        key = expansion, tile
        if key not in self.edge_dicts:
            self.edge_dicts[key] = build_edge_dict(expansion, tile)
        return self.edge_dicts[key]

    def neighbour_table(self, expansion, tile):
        key = expansion, tile
        if key not in self.neighbour_tables:
            self.neighbour_tables[key] = build_neighbour_table(
                self.edge_dict(expansion, tile))
        return self.neighbour_tables[key]

    def build_all_neighbour_tables(self):
        for expansion in self.tiling.expansions.values():
            for tile in expansion.parentlayer.tiles:
                self.neighbour_table(expansion, tile)

    def meeting_tile_edges(self):
        if self.mte is None:
            self.build_mte_and_mas()
        return self.mte

    def meetings_across_spurs(self):
        if self.mas is None:
            self.build_mte_and_mas()
        return self.mas

    def build_mte_and_mas(self):
        # Construct the graph of which (tile, edge) pairs can be adjacent.
        # You expect this to be constrained by edge type (e.g. in the P2
        # triangles tiling, A edge #1 and A edge #2 can't connect because
        # they have different formal edge type), but there may be further
        # constraints (e.g. V edge #0 and U edge #1 can't connect _even
        # though_ they have the same formal edge type).
        #
        # This function currently only returns pairs that are _directly_
        # adjacent, i.e. not separated by a zero-thickness spur of another
        # tile expansion.

        # Output dict. This maps each (tile, edge index) pair to the set
        # of pairs that can meet it.
        self.mte = collections.defaultdict(set)
        self.mas = collections.defaultdict(set)
        self.build_all_neighbour_tables()
        total = -1
        while True:
            self.iterate_mte_build()
            self.iterate_mas_build()
            oldtotal, total = (
                total,
                sum(map(len, self.mte)) + sum(map(len, self.mas)))
            if oldtotal == total:
                break

    def iterate_mte_build(self):
        done_something = False
        def discover(t1, i1, t2, i2):
            nonlocal done_something
            lengths = len(self.mte[t1, i1]), len(self.mte[t2, i2])
            self.mte[t1, i1].add((t2, i2))
            self.mte[t2, i2].add((t1, i1))
            if (len(self.mte[t1, i1]), len(self.mte[t2, i2])) != lengths:
                done_something = True

        # Tile edges that meet in the interior of any expansion are equivalent
        for (expansion, tile), neighbours in self.neighbour_tables.items():
            for n1, n2 in neighbours.items():
                if n1[0] == n2[0] == 'int':
                    c1, i1 = n1[1:]
                    c2, i2 = n2[1:]
                    t1 = expansion.tiles[tile][c1][0]
                    t2 = expansion.tiles[tile][c2][0]
                    discover(t1, i1, t2, i2)

        # For each pair of tile edges that can meet, tile edges that meet
        # along any expansion of those edges can also meet. Iterate this
        # to closure.
        while True:
            done_something = False

            for (t1, i1), edges2 in list(self.mte.items()):
                for t2, i2 in list(edges2):
                    # Determine the type of the edges meeting between
                    # these tiles, which we expect to be consistent.
                    edgetype = self.tiling.tile_edges[t1][i1]
                    assert self.tiling.tile_edges[t2][i2] == (
                        edgetype[0], -edgetype[1])
                    for expansion in self.tiling.expansions.values():
                        nt1 = self.neighbour_tables.get((expansion, t1))
                        nt2 = self.neighbour_tables.get((expansion, t2))
                        if nt1 is not None and nt2 is not None:
                            edgelen = len(expansion.edge_edges[edgetype])
                            for i in range(edgelen):
                                k1, hi1, lo1 = nt1.get(('ext', i1, i))
                                k2, hi2, lo2 = nt2.get(('ext', i2, edgelen-1-i))
                                if k1 == k2 == 'int':
                                    ct1 = expansion.tiles[t1][hi1][0]
                                    ct2 = expansion.tiles[t2][hi2][0]
                                    discover(ct1, lo1, ct2, lo2)
                                # if either of k1,k2 is 'ext' then here we would
                                # need to process spurs in some way

            # For any tile edges in a pair of expansions that can meet
            # across spurs, the corresponding base tile edges can also meet.
            for expansion, t1, t2, hi1, lo1, hi2, lo2 in self.mas:
                nt1 = self.neighbour_tables.get((expansion, t1))
                nt2 = self.neighbour_tables.get((expansion, t2))
                k1, hi1, lo1 = nt1.get(('ext', hi1, lo1))
                k2, hi2, lo2 = nt2.get(('ext', hi2, lo2))
                if k1 == k2 == 'int':
                    ct1 = expansion.tiles[t1][hi1][0]
                    ct2 = expansion.tiles[t2][hi2][0]
                    discover(ct1, lo1, ct2, lo2)

            if not done_something:
                break # no progress in this iteration

    def iterate_mas_build(self):
        def poly_edges_meeting(poly1, poly2):
            "Return pairs (i,j) such that poly1 edge #i meets poly2 edge #j."
            vpairs2 = {(poly2.vertex(j), poly2.vertex(j+1)): j
                       for j in range(poly2.order)}
            for i in range(poly1.order):
                vpair1 = (poly1.vertex(i+1), poly1.vertex(i))
                if vpair1 in vpairs2:
                    yield i, vpairs2[vpair1]

        for (t1, i1), edges2 in list(self.mte.items()):
            edgetype = self.tiling.tile_edges[t1][i1]

            for t2, i2 in list(edges2):
                assert self.tiling.tile_edges[t2][i2] == (
                    edgetype[0], -edgetype[1])

                # First, determine which other edges of the unexpanded
                # polygons meet, if we place edges i1 and i2 adjacent.
                poly1 = self.tiling.polygon_from_outline(
                    self.tiling.tile_outline(t1)[0])
                poly2 = self.tiling.polygon_from_outline(
                    self.tiling.tile_outline(t2)[0])
                poly2.place_next_to_edge(i2, poly1, i1)
                boring_edge_pairs = set(poly_edges_meeting(poly1, poly2))

                for expansion in self.tiling.expansions.values():
                    if not (t1 in expansion.tiles and t2 in expansion.tiles):
                        continue

                    # Get the outline of each tile's expansion
                    outline1 = expansion.expand_outline(
                        self.tiling.tile_outline(t1)[0])
                    outline2 = expansion.expand_outline(
                        self.tiling.tile_outline(t2)[0])

                    # Convert them into polygons
                    poly1 = self.tiling.polygon_from_outline(outline1)
                    poly2 = self.tiling.polygon_from_outline(outline2)

                    # Find the range of edge indices of the expanded
                    # outline that correspond to the original tile edges
                    # in question.
                    edgelen = len(expansion.edge_edges[edgetype])
                    pi1 = sum(len(expansion.edge_edges[e])
                              for e in self.tiling.tile_edges[t1][:i1])
                    pi2 = sum(len(expansion.edge_edges[e])
                              for e in self.tiling.tile_edges[t2][:i2])

                    # Place the polygons next to each other so that one
                    # pair of those sub-edges meet.
                    poly2.place_next_to_edge(pi2, poly1, pi1 + edgelen - 1)

                    # Check that this caused the whole edge expansion to
                    # meet.
                    for i in range(edgelen + 1):
                        assert (poly1.vertex(pi1 + edgelen - i) ==
                                poly2.vertex(pi2 + i)), (
                                    f"erk {t1}{i1}/{t2}{i2}/{i}")

                    # Now find out what _other_ edges of the two
                    # expansions meet.
                    for i, j in poly_edges_meeting(poly1, poly2):
                        hi1, lo1 = poly1.edgelabels[i]
                        hi2, lo2 = poly2.edgelabels[j]

                        if (hi1, hi2) in boring_edge_pairs:
                            # This case might be boring, because it's part
                            # of an edge pair that met already before the
                            # expansion.
                            #
                            # But check that the edge sub-indices match,
                            # before we jump to that conclusion.
                            etype = self.tiling.tile_edges[t1][hi1]
                            assert self.tiling.tile_edges[t2][hi2] == (
                                etype[0], -etype[1])
                            elen = len(expansion.edge_edges[etype])
                            if lo1 + lo2 == elen - 1:
                                continue # ok, this is boring

                        # An interesting case! Record the fact that these
                        # two edges of this tile expansion can meet
                        # geometrically without meeting combinatorially,
                        # and if so, it's because of a different pair of
                        # edges of the supertiles that do meet
                        # combinatorially.
                        self.mas[expansion, t1, t2, hi1, lo1, hi2, lo2].add(
                            (i1, i2))

    def limiting_probabilities(self):
        if self.probs is not None:
            return self.probs

        # For each expansion, a matrix that takes a vector indicating
        # the number of each tile type in the parent layer, and
        # delivers a vector indicating how many of each tile type in
        # the child layer those expand to.
        exp_matrices = {}

        for expansion in self.tiling.expansions.values():
            np = len(expansion.parentlayer.tiles)
            nc = len(expansion.childlayer.tiles)
            matrix_values = [0] * (np * nc)

            pindices = {tile: i for i,tile in enumerate(
                sorted(expansion.parentlayer.tiles))}
            cindices = {tile: i for i,tile in enumerate(
                sorted(expansion.childlayer.tiles))}

            for parent, children in expansion.tiles.items():
                for index, (child, *_) in children.items():
                    matrix_values[cindices[child] * np + pindices[parent]] += 1

            exp_matrices[expansion] = matrix(QQbar, nc, np, matrix_values)

        # Now find a cycle in the layers
        layerlist = []
        layerpos = {}
        def append_layer(layer):
            layerpos[layer.name] = len(layerlist)
            layerlist.append(layer)
        append_layer(self.tiling.layers[0])
        for i in itertools.count(1):
            next_layer = self.tiling.layers_by_name[layerlist[-1].successor]
            if next_layer.name in layerpos:
                cyclestart = layerpos[next_layer.name]
                cycleend = len(layerlist)
                layerlist.append(next_layer)
                repeated_layer = next_layer
                break
            append_layer(next_layer)

        # Multiply the matrices within that cycle to find the matrix
        # that maps an instance of repeated_layer to the next instance.
        m = identity_matrix(QQbar, len(repeated_layer.tiles))
        for i in reversed(range(cyclestart, cycleend)):
            exp = self.tiling.expansions[layerlist[i].name,
                                         layerlist[i+1].name]
            m = exp_matrices[exp] * m

        # Now we have the matrix that is iterated every time
        # repeated_layer expands back to itself. Find its eigenvalues.
        evs = m.eigenvectors_right() # list of (eigenval, [eigenvecs], algmult)
        max_ev = max(evs, key = lambda t: t[0])

        # The eigenvector corresponding to that maximum eigenvalue had
        # better be unique, or we have a more difficult problem on our
        # hands...
        assert len(max_ev[1]) == 1, "help, multiple eigenvectors!"

        # Now we can start accumulating distribution vectors. Scale
        # each one so that the sum of its entries is 1, i.e. they're
        # actually probabilities.
        unit_length = lambda v: v / sum(v)
        dists = {repeated_layer.name: unit_length(max_ev[1][0])}

        # Fill in every layer's distribution vector by multiplying its
        # matrix into the vector for its successor layer.
        #
        # This will overwrite repeated_layer when we get to it, but we
        # don't care - it should overwrite it with the same thing
        # anyway, so there's no point avoiding it.
        for i in reversed(range(cycleend)):
            layer0, layer1 = layerlist[i:i+2]
            expansion = self.tiling.expansions[layer1.name, layer0.name]
            dists[layer0.name] = unit_length(
                exp_matrices[expansion] * dists[layer1.name])

        # Finally, convert those integer-indexed vectors back into
        # dicts indexed by tile name.
        self.probs = {}
        for layer in layerlist:
            vec = dists[layer.name]
            self.probs[layer.name] = {
                tile: vec[i]
                for i,tile in enumerate(sorted(layer.tiles))
            }

        return self.probs
