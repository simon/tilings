# -*- python -*-

import collections
import functools
import itertools
import html

from dsf import DSF
import recursive
import util

class Comparable:
    """Group a set of namedtuples into a supertype that makes them compare
    as if they each include their type name, i.e. no subthings of distinct
    types are equal."""
    def _astuple(self):
        return (self.__class__.__bases__[1].__name__,) + tuple(self)
    def __hash__(self): return hash(self._astuple())
    def __eq__(self, rhs): return self._astuple() == rhs._astuple()
    def __ne__(self, rhs): return self._astuple() != rhs._astuple()
    def __lt__(self, rhs): return self._astuple() <  rhs._astuple()
    def __le__(self, rhs): return self._astuple() <= rhs._astuple()
    def __gt__(self, rhs): return self._astuple() >  rhs._astuple()
    def __ge__(self, rhs): return self._astuple() >= rhs._astuple()

class Start(Comparable, collections.namedtuple("Start", [])): pass
class Accept(Comparable, collections.namedtuple(
        "Accept", ["layername", "tile"])): pass
class Coords(Comparable, collections.namedtuple(
        "Coords", ["layername", "ic", "oc"])): pass

class AdjacencyMatcher:
    def __init__(self, tiling, base_layer=None):
        self.tiling = tiling
        self.base_layer = (base_layer if base_layer is not None
                           else self.tiling.layers[0])

        self.generated = False # has generate() been called?
        self.paths_from_start_cache = None
        self.paths_to_accept_cache = None

    class Builder:
        "Inner class to hold the temporary machinery to make the state machine"

        def __init__(self, am, verbose):
            self.am = am
            self.verbose = verbose
            self.tiling = am.tiling
            self.base_layer = am.base_layer
            self.dsf = DSF()
            self.trans_from = collections.defaultdict(set) # src -> [(sym,dst)]
            self.trans_into = collections.defaultdict(set) # dst -> [(sym,src)]
            self.next_prefixes = [[]]
            self.accept_layers = set()

            # Follow links to successor layers until we can predict what layer
            # will be found at any depth
            layers = []
            layers_seen = {}
            layer = self.base_layer
            while True:
                if layer.name in layers_seen:
                    repstart = layers_seen[layer.name]
                    repend = len(layers)
                    replen = repend - repstart
                    break
                layers_seen[layer.name] = len(layers)
                layers.append(layer)
                layer = self.tiling.layers_by_name[layer.successor]
            self.layer_at_depth = lambda n: layers[
                n if n < repend else repstart + (n-repstart) % replen
            ]
            self.expansion_at_depth = lambda n: self.tiling.expansions[
                self.layer_at_depth(n+1).name, self.layer_at_depth(n).name
            ]

        def iter_paths(self, depth, prefix=[]):
            if depth <= len(prefix):
                if depth == len(prefix):
                    yield prefix
                return

            if len(prefix) == 0:
                for tile in self.base_layer.tiles:
                    for iedge in range(len(self.tiling.tile_edges[tile])):
                        yield from self.iter_paths(
                            depth, prefix=[(tile, iedge)])
            else:
                expansion = self.expansion_at_depth(len(prefix)-1)
                for tile, index in expansion.permitted_parents[prefix[-1][0]]:
                    yield from self.iter_paths(
                        depth, prefix=prefix+[(tile, index)])

        @staticmethod
        def trim(string):
            return tuple([string[0][0]] + string[1:])

        def add_transitions_for(self, istring, ostring):
            assert len(istring) == len(ostring)

            layer = self.base_layer
            self.add_trans(
                Start(),
                (istring[0], ostring[0]),
                Coords(layer.name, self.trim(istring), self.trim(ostring)))

            for j in range(len(istring)-2):
                succ = self.tiling.layers_by_name[layer.successor]
                self.add_trans(
                    Coords(layer.name, self.trim(istring[j:]),
                           self.trim(ostring[j:])),
                    (istring[j+1], ostring[j+1]),
                    Coords(succ.name, self.trim(istring[j+1:]),
                           self.trim(ostring[j+1:])))
                layer = succ

            succ = self.tiling.layers_by_name[layer.successor]
            assert istring[-1][0] == ostring[-1][0]
            self.add_trans(
                Coords(layer.name, self.trim(istring[-2:]),
                       self.trim(ostring[-2:])),
                (istring[-1], ostring[-1]),
                Accept(succ.name, istring[-1][0]))
            self.add_accept_transitions(succ.name)

        def make_paths(self, depth):
            ctx = recursive.InextensibleAddressContext(
                self.tiling, self.base_layer.name)

            old_prefixes = self.next_prefixes
            self.next_prefixes = []

            for prefix in old_prefixes:
                for istring in self.iter_paths(depth, prefix):
                    base, iedge = istring[0]
                    addr = recursive.RecursiveAddress(ctx, base, istring[1:])
                    try:
                        oedge = addr.step(iedge)
                    except util.InextensibleAddressException:
                        self.next_prefixes.append(istring)
                        continue
                    ocoords = addr.coords()
                    ostring = [(ocoords[0], oedge)] + ocoords[1:]
                    if self.verbose:
                        print("add input:", istring, "->", ostring)
                    self.add_transitions_for(istring, ostring)

        def add_trans(self, src, sym, dst):
            self.dsf.register(src)
            self.dsf.register(dst)
            self.trans_from[src].add((sym, dst))
            self.trans_into[dst].add((sym, src))
            #print("trans", src, sym, dst)

        def add_accept_transitions(self, layername):
            if layername in self.accept_layers:
                return
            self.accept_layers.add(layername)
            layer = self.tiling.layers_by_name[layername]
            succ = self.tiling.layers_by_name[layer.successor]
            exp = self.tiling.expansions[succ.name, layer.name]
            for supertile, children in exp.tiles.items():
                for idx, (subtile, *_) in children.items():
                    self.add_trans(Accept(layer.name, subtile),
                                   ((supertile, idx),) * 2,
                                   Accept(succ.name, supertile))

        def merge_states(self):
            while True:
                to_unify = []

                for src_class in self.dsf.classes():
                    bysym = collections.defaultdict(set)
                    for src in src_class:
                        for sym, dst in self.trans_from[src]:
                            if isinstance(dst, (Coords, Accept)):
                                bysym[sym, dst.layername].add(dst)
                    for descs in bysym.values():
                        to_unify.append(descs.copy())

                for dst_class in self.dsf.classes():
                    bysym = collections.defaultdict(set)
                    for dst in dst_class:
                        for sym, src in self.trans_into[dst]:
                            if isinstance(src, (Coords, Accept)):
                                bysym[sym, src.layername].add(src)
                    for descs in bysym.values():
                        to_unify.append(descs.copy())

                done_something = False
                for descs in to_unify:
                    #print("unify", descs)
                    if self.dsf.unify_all(descs):
                        done_something = True
                if not done_something:
                    break

                new_trans_from = collections.defaultdict(set)
                new_trans_into = collections.defaultdict(set)
                for src, transitions in self.trans_from.items():
                    csrc = self.dsf.canonical(src)
                    for sym, dst in transitions:
                        cdst = self.dsf.canonical(dst)
                        new_trans_from[csrc].add((sym, cdst))
                        new_trans_into[cdst].add((sym, csrc))
                self.trans_from = new_trans_from
                self.trans_into = new_trans_into

        def output(self):
            #print("got", sum(1 for _ in self.dsf.classes()))

            def sort_key(thing):
                if isinstance(thing, Start):
                    return (0, thing)
                if isinstance(thing, Accept):
                    return (1, thing)
                if isinstance(thing, Coords):
                    return (2, len(thing.ic), thing)
                raise ValueError(f"sort key for bad {thing!r}")

            states = sorted(
                ((min(cl, key=sort_key), cl) for cl in self.dsf.classes()),
                key=lambda t: sort_key(t[0]))
            self.am.state_descs = []
            indices = {}
            for representative, items in states:
                #print("state", repr(representative), repr(items))
                j = len(self.am.state_descs)
                self.am.state_descs.append(representative)
                for item in items:
                    indices[item] = j

            transitions = {}
            for src, symdsts in self.trans_from.items():
                srcindex = indices[src]
                for sym, dst in symdsts:
                    dstindex = indices[dst]
                    transitions.setdefault(srcindex, set()).add((sym, dstindex))
            self.am.transitions = {
                srcindex: sorted(things)
                for srcindex, things in transitions.items()
            }

            self.am.start = indices[Start()]

            self.am.accepting = set(index for desc, index in indices.items()
                                    if isinstance(desc, Accept))

    def generate(self, verbose=False):
        builder = self.Builder(self, verbose)
        for depth in itertools.count(2):
            if verbose:
                print("depth", depth)
            builder.make_paths(depth)
            builder.merge_states()
            builder.output()
            unaccepted = self.find_unaccepted_input()
            if not unaccepted:
                if verbose:
                    print("done!")
                break
            else:
                if verbose:
                    print("still unaccepted:", unaccepted)

    def is_accepting(self, index):
        return index in self.accepting

    def winnow(self):
        pass # no longer needed, but kept for backward compat

    @property
    def paths_from_start(self):
        if self.paths_from_start_cache is not None:
            return self.paths_from_start_cache

        paths = {}
        queue = []
        def discover(state, prev):
            if state in paths:
                return
            paths[state] = paths.get(prev, []) + [state]
            queue.append(state)

        discover(self.start, None)
        while len(queue) > 0:
            src = queue.pop(0)
            for sym, dst in self.transitions[src]:
                discover(dst, src)

        self.paths_from_start_cache = paths
        return self.paths_from_start_cache

    @property
    def paths_to_accept(self):
        if self.paths_to_accept_cache is not None:
            return self.paths_to_accept_cache

        predecessors = collections.defaultdict(set)
        for src, trans in self.transitions.items():
            for sym, dst in trans:
                predecessors[dst].add(src)

        paths = {}
        queue = []
        def discover(state, prev):
            if state in paths:
                return
            paths[state] = [state] + paths.get(prev, [])
            queue.append(state)

        for acc in self.accepting:
            discover(acc, None)
        while len(queue) > 0:
            dst = queue.pop(0)
            for src in predecessors[dst]:
                discover(src, dst)

        self.paths_to_accept_cache = paths
        return self.paths_to_accept_cache

    def find_unaccepted_inputs(self):
        # After generating an adjacency matcher, find input string
        # prefixes for which it doesn't have any potential output.

        lm = LegalityMatcher(self.tiling, self.base_layer)

        visited = {}
        queue = []

        def discover(string, pair):
            if pair not in visited:
                visited[pair] = string
                queue.append(pair)

        discover((), (frozenset([self.start]), lm.start))

        while len(queue) > 0:
            amstates, lmstate = pair = queue.pop(0)
            string = visited[pair]
            amtrans = collections.defaultdict(set)
            for amstate in amstates:
                for amsym, dest in self.transitions[amstate]:
                    amtrans[amsym[0]].add(dest)
            lmtrans = lm.transitions(lmstate)
            amset = set(amtrans)
            lmset = set(lmtrans)
            extra_syms = amset.difference(lmset)
            assert len(extra_syms) == 0, (
                "Adjacency matcher accepts nonsense: {}".format(
                    string + (next(iter(extra_syms)),)
                ))
            if amset != lmset:
                missing_sym = next(iter(lmset.difference(amset)))
                yield string + (missing_sym,)

            for sym in sorted(lmset):
                discover(string + (sym,),
                         (frozenset(amtrans[sym]), lmtrans[sym]))

    def find_unaccepted_input(self):
        return next(itertools.chain(self.find_unaccepted_inputs(), [None]))

    def check_string(self, string):
        states = {self.start}
        for sym in string:
            states = {dst for src in states
                      for tsym, dst in self.transitions[src]
                      if tsym == sym}
            if len(states) == 0:
                return False
        return any(self.is_accepting(state) for state in states)

class LegalityMatcher:
    def __init__(self, tiling, base_layer=None):
        self.tiling = tiling
        self.base_layer = (base_layer if base_layer is not None
                           else self.tiling.layers[0])
        self.start = Start()
        self.transition_cache = {}
        self.states = set()

    def transitions(self, state):
        # Top-level function to work out the transitions from a half-state.
        if state not in self.transition_cache:
            if isinstance(state, Start):
                gen = self.transitions_start()
            elif isinstance(state, Accept):
                gen = self.transitions_accept(state)
            else:
                assert False, f"bad symbol {repr(state)} in LegalityMatcher"
            self.transition_cache[state] = dict(gen)
        return self.transition_cache[state]

    def transitions_start(self):
        for tile in self.base_layer.tiles:
            for edge in range(len(self.tiling.tile_edges[tile])):
                yield (tile, edge), Accept(self.base_layer.name, tile)

    def transitions_accept(self, state):
        parentlayername = self.tiling.layers_by_name[state.layername].successor
        expansion = self.tiling.expansions[parentlayername, state.layername]
        for parent_tile, children in expansion.tiles.items():
            for child_index, (child_tile, *_) in children.items():
                if child_tile == state.tile:
                    yield (parent_tile, child_index), Accept(
                        parentlayername, parent_tile)

class NonClosure(Exception):
    def __init__(self, srcdesc, dstdesc, syms):
        self.srcdesc = srcdesc
        self.dstdesc = dstdesc
        self.syms = syms

class MappingFailed(Exception):
    def __init__(self, state, sym):
        self.state = state
        self.sym = sym

class NonAccept(Exception):
    def __init__(self, string):
        self.string = string

class Transducer:
    def __init__(self, adjmatcher, debug=False):
        self.am = adjmatcher

        # Primary output fields describing the transducer, which will
        # all be filled in by build().

        # States are indexed by non-negative integers.
        # self.trans[state] is a dict of the transitions for a given
        # state, mapping an input symbol to (destination state, tuple
        # of output symbols).
        self.trans = {}

        # Every state of the transducer corresponds to a set of pairs
        # (AdjacencyMatcher state, tuple of pending output symbols).
        # For diagnostic purposes, self.desc[state] records that data.
        self.desc = {}

        # For each state of the transducer, record at least one way to
        # reach it from a previous state. Following these links back,
        # one can construct a path to any state from the start state.
        # Useful for diagnostics: if something odd happens in a state,
        # we can identify an input that might be affected.
        self.pred = {} # state -> (previous state, input symbol, output tuple)

        # Starting state, and which states are accepting.
        #
        # An accepting state still has transitions, because its job is
        # to receive any valid continuation of the input tile address,
        # and emit it unchanged. But for some applications you prefer
        # to know you've reached such a state, and stop.
        self.start = None
        self.accept = set()

        self.built = False

    def build(self):
        if self.built:
            return
        self.built = True

        indices = itertools.count()
        desc_to_index = {}
        desc_queue = []

        def common_prefix_len(a, b):
            outlen = 0
            for i in range(min(len(a), len(b)) + 1):
                if a[:i] == b[:i]:
                    outlen = i
                else:
                    break
            return outlen

        def factor_output(desc):
            # Find output symbols common to all states in this desc.
            it = iter(desc)
            state, common_output = next(it)
            for state, output in it:
                cplen = common_prefix_len(common_output, output)
                common_output = common_output[:cplen]

            cplen = len(common_output)
            newdesc = frozenset((state, output[cplen:])
                                for state, output in desc)
            return newdesc, common_output

        def discover(desc, pred_index, i, o):
            # When we've just explored a state transition and found
            # 'desc' at the end of it, this function adds it to the
            # list of known states and queues it for future
            # exploration, if it wasn't already known.

            if desc in desc_to_index:
                return desc_to_index[desc]
            index = next(indices)
            desc_to_index[desc] = index
            self.desc[index] = desc
            self.pred[index] = pred_index, i, o
            desc_queue.append(desc)

            if pred_index is None:
                # Special case that means this is the start state
                self.start = index

            if any(self.am.is_accepting(state) for state, output in desc):
                # I don't think we ever expect a transducer state to
                # combine an accepting with a non-accepting state of
                # the adjacency matcher
                assert all(self.am.is_accepting(state)
                           and len(output) == 0
                           for state, output in desc)
                self.accept.add(index)

            return index

        discover(frozenset({(self.am.start, ())}), None, None, None)
        next_cycle_check = 16
        while len(desc_queue) > 0:
            src_desc = desc_queue.pop(0)
            src_index = desc_to_index[src_desc]

            # Explore all the transitions from states in desc, and
            # collect them together into sets indexed by the first
            # element of the symbol pair. This tells us, for a given
            # input symbol, what set of states we can reach from this
            # set of states, generating what extra output.
            these_transitions = collections.defaultdict(set)
            for state, output in src_desc:
                for (sym1, sym2), dest in self.am.transitions[state]:
                    these_transitions[sym1].add((dest, output + (sym2,)))

            # Now each element of these_transitions is a DFA transition.
            self.trans[src_index] = {}
            for sym, dest_protodesc in these_transitions.items():
                dest_desc, output = factor_output(dest_protodesc)
                dest_index = discover(dest_desc, src_index, sym, output)
                self.trans[src_index][sym] = dest_index, output

            if src_index >= next_cycle_check:
                self.check_empty_loops()
                next_cycle_check *= 2

    def path_to(self, state):
        path = []
        while state != self.start:
            state, i, o = self.pred[state]
            path.append((state, i, o))
        path.reverse()
        return path

    def path_diag(self, state):
        return "".join(itertools.chain(
            (f"From {s}, input {i}, output {o}\n"
             for s, i, o in self.path_to(state)),
            [f"Arrive in {state}\n"]
        ))

    def check_empty_loops(self):
        # Search the transducer automaton (even if it's only half
        # built) for any cycle of edges that emit no output and return
        # to a state with the same 'core', defined as the set of
        # AdjacencyMatcher states without considering the pending
        # output.
        #
        # This is an indication that the automaton can't possibly
        # close up, because that cycle could be traversed more and
        # more times accumulating more copies of the same output, so
        # the total number of states must be infinite.
        #
        # Method: depth-first search.

        visited = set()

        def vertex_core(index):
            return frozenset(state for (state, output) in self.desc[index])

        def dfs_visit(index, stack, stackpos):
            core = vertex_core(index)

            if core in stackpos:
                pos, prev_index = stackpos[core]
                raise NonClosure(self.desc[prev_index], self.desc[index],
                                 stack[pos:])

            if index not in visited:
                visited.add(index)

                stackpos[core] = len(stack), index

                transitions = self.trans.get(index, {})
                for sym, (dest, output) in transitions.items():
                    if len(output) == 0:
                        stack.append((sym))
                        dfs_visit(dest, stack, stackpos)
                        stack.pop()

                del stackpos[core]

        for index in sorted(self.desc):
            dfs_visit(index, [], {})

    def find_unaccepted_input(self):
        # After generating a transducer, determine whether it can
        # handle every possible legal input tile address.

        lm = LegalityMatcher(self.am.tiling, self.am.base_layer)

        visited = {}
        queue = []

        def discover(string, pair):
            if pair not in visited:
                visited[pair] = string
                queue.append(pair)

        discover((), (self.start, lm.start))

        while len(queue) > 0:
            tstate, lmstate = pair = queue.pop(0)
            string = visited[pair]
            ttrans = self.trans[tstate]
            lmtrans = lm.transitions(lmstate)
            tset = set(ttrans)
            lmset = set(lmtrans)
            assert tset.issubset(lmset), "Transducer accepts nonsense?!"
            if tset != lmset:
                missing_sym = next(iter(lmset.difference(tset)))
                return string + (missing_sym,)

            for sym in sorted(lmset):
                discover(string + (sym,), (ttrans[sym][0], lmtrans[sym]))

        return None

    def map_string_segment(self, state, string, accepted=lambda:None):
        output = ()
        for sym in string:
            try:
                state, out = self.trans[state][sym]
                output += out
            except KeyError:
                raise MappingFailed(state, sym)
            if state in self.accept:
                accepted()
        return state, output

    def map_finite_string(self, string, accepted=lambda:None):
        state, output = self.map_string_segment(self.start, string, accepted)
        if state not in self.accept:
            raise NonAccept(string)
        return output

    def map_repeating_string(self, initial, repeating, accepted=lambda:None):
        state = self.start
        state, new_initial = self.map_string_segment(
            state, initial, accepted)
        segments = [new_initial]

        statepos = {}
        while state not in statepos:
            statepos[state] = len(segments)
            state, new_repeating = self.map_string_segment(
                state, repeating, accepted)
            segments.append(new_repeating)

        oldpos = statepos[state]
        output_initial = functools.reduce(lambda x,y: x+y, segments[:oldpos])
        output_repeating = functools.reduce(lambda x,y: x+y, segments[oldpos:])
        return output_initial, output_repeating

    def emit_graphviz(self, fh):
        def pr(*args, **kws):
            print(*args, **kws, file=fh)

        pr("#", self.accept)
        pr("digraph {")
        states = sorted(
            (state in self.accept, state, transitions)
            for state, transitions in self.trans.items()
        )
        for accepting, state, _ in states:
            pr("{} [shape={}];".format(
                state, "doublecircle" if accepting else "circle"))
        for _, src, transitions in states:
            for sym, (dest, output) in transitions.items():
                label = html.escape(self.am.tiling.display_coord(sym))
                if len(output) > 0:
                    label += " → <FONT COLOR=\"red\">{}</FONT>".format(
                        html.escape(" ".join(
                            map(self.am.tiling.display_coord, output))))
                pr(f"{src} -> {dest} [label = <{label}>];")
        pr("}")

class FakeTransducer:
    # Implements the map_repeating_string method of Transducer,
    # without having an actual transducer, only an adjacency matcher.
    # Works by combining the input coordinate string with the
    # adjmatcher DFA, to constrain one side of the latter's input to
    # the form. This produces a custom DFA for this single coordinate
    # transition, matching all the _possible_ output strings. Then we
    # check if the result is unique, and either return the unique
    # answer or throw InfiniteAmbiguityException.

    def __init__(self, adjmatcher):
        self.am = adjmatcher

    def map_repeating_string(self, initial, repeating, accepted=lambda:None):
        # Directly implement an output-only DFA that generates the
        # infinite input string
        ni = len(initial)
        nr = len(repeating)
        def input_trans(state):
            if state < ni:
                return state + 1, initial[state]
            else:
                k = state - ni
                return ni + (k+1) % nr, repeating[k]

        # Machinery for building the intersection DFA
        succs = {} # state -> {successor state -> output symbol}
        preds = {} # state -> set of predecessors
        state_queue = []
        def discover(amstate, instate, prevstate, output):
            state = (amstate, instate)
            if state not in succs:
                succs[state] = {}
                preds[state] = set()
                state_queue.append(state)
            if prevstate is not None:
                succs[prevstate][state] = output
                preds[state].add(prevstate)

        # Actually build the intersection
        discover(self.am.start, 0, None, None)
        start = state_queue[0]
        while len(state_queue) > 0:
            state = state_queue.pop(0)
            amstate, instate = state
            insucc, insym = input_trans(instate)
            for (sym1, sym2), amsucc in self.am.transitions[amstate]:
                if sym1 == insym:
                    discover(amsucc, insucc, state, sym2)

        # Prune the resulting DFA to remove nodes with no successor
        prune_queue = [state for state, trans in succs.items()
                       if len(trans) == 0]
        while len(prune_queue) > 0:
            state = prune_queue.pop(0)
            for pred in preds[state]:
                del succs[pred][state]
                if len(succs[pred]) == 0:
                    prune_queue.append(pred)

        # Now read off the output
        output = []
        state = start
        lastseen = {}
        seen_accept = False
        while True:
            # Check if we saw an accepting state
            if self.am.is_accepting(state[0]):
                seen_accept = True

            # Follow the unique next transition, unless it's not unique
            transitions = succs[state]
            if len(transitions) != 1:
                raise util.InfiniteAmbiguityException()
            state, outsym = next(iter(transitions.items()))
            output.append(outsym)

            # See if we've encountered a loop
            if state in lastseen:
                # Yes, so we'll repeat everything between here and the
                # last time we were here
                lastpos = lastseen[state]
                if seen_accept:
                    accepted()
                return output[:lastpos], output[lastpos:]
            lastseen[state] = len(output)
