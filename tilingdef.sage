# -*- python -*-

import collections

import util

Layer = collections.namedtuple(
    "Layer", ["name", "tiles", "successor"])

def edge_types(outline):
    for element in outline:
        if element[0] == 'edge':
            yield element[1:]

def reverse_outline(outline):
    for element in reversed(outline):
        if element[0] == "turn":
            yield element[0], 1/element[1]
        elif element[0] == "edge":
            yield element[0], element[1], -element[2]

class Tiling:
    def __init__(self):
        self.edges = {} # edge name -> (vector, is_signed)
        self.tiles = {} # tile name -> (outline, preferred orientation)
        self.tile_colours = {} # tile name -> preferred colour, if any
        self.tile_arrows = {} # tile name -> fn(vertices -> arrow endpoints)
        self.tile_edges = {} # tile name -> list of edge types
        self.layers = [] # list of Layer
        self.layers_by_name = {} # layer name -> Layer
        self.expansions = {} # (parent layer, child layer) -> Expansion

    class Expansion:
        def __init__(self, tiling, parentlayer, childlayer):
            self.is_reversing = False
            self.tiling = tiling
            self.parentlayer = parentlayer
            self.childlayer = childlayer
            self.edges = {} # (edge, sign) -> outline fragment
            self.edge_edges = {} # (edge, sign) -> list of edge types

            # tile name -> (child idx -> (tile type, existing edge, child edge)
            self.tiles = collections.defaultdict(collections.OrderedDict)

            # child tile name -> set of (parent tile name, child index)
            self.permitted_parents = {tile: set() for tile in childlayer.tiles}

            # Overrides to base tile colour when drawing a particular expansion
            # tile name -> (child idx -> preferred colour, if any)
            self.child_colours = collections.defaultdict(dict)

        def _identity(self):
            return self.parentlayer.name, self.childlayer.name
        def __hash__(self):
            return hash(('Expansion', self._identity()))
        def __repr__(self):
            return (f"<Expansion {self.parentlayer.name} -> "
                    f"{self.childlayer.name}>")
        def __eq__(self, rhs): return self._identity() == rhs._identity()
        def __ne__(self, rhs): return self._identity() != rhs._identity()
        def __le__(self, rhs): return self._identity() <= rhs._identity()
        def __ge__(self, rhs): return self._identity() >= rhs._identity()
        def __lt__(self, rhs): return self._identity() <  rhs._identity()
        def __gt__(self, rhs): return self._identity() >  rhs._identity()

        def set_edge(self, signed_edge, outline):
            outline = list(outline)
            self.edges[signed_edge] = outline
            self.edge_edges[signed_edge] = list(edge_types(outline))

        def expand_outline(self, outline):
            for element in outline:
                if element[0] == 'edge':
                    yield ('push',)
                    yield from iter(self.edges[element[1:]])
                    yield ('pop',)
                elif element[0] == 'turn' and self.is_reversing:
                    yield element[0], 1/element[1]
                else:
                    yield element

        def expand_tile(self, tile):
            outline, orientation = self.tiling.tile_outline(tile)
            outline = self.expand_outline(outline)
            outline_polygon = self.tiling.polygon_from_outline(
                outline, orientation)
            ext_edges = {label: i for i, label in
                         outline_polygon.edgelabels.items()}
            child_polygons = {}
            for child_index, (child_type, (existing_edge_type, hi, lo),
                              child_edge) in self.tiles[tile].items():
                if existing_edge_type == 'ext':
                    vpoly = outline_polygon
                    vindex = ext_edges[hi, lo]
                    reverse = not self.is_reversing
                elif existing_edge_type == 'int':
                    vpoly = child_polygons[hi]
                    vindex = lo
                    reverse = False
                else:
                    assert False, f"bad edge type {existing_edge_type!r}"
                child_outline, _ = self.tiling.tile_outline(child_type)
                child_polygon = self.tiling.polygon_from_outline(child_outline)
                child_polygon.place_next_to_edge(
                    child_edge, vpoly, vindex, reverse)
                child_polygons[child_index] = child_polygon
            return outline_polygon, child_polygons, orientation

    # API provided to the tiling definition code for constructing the tiling

    def signed_edge(self, name, vector):
        if name in self.edges:
            raise ValueError(f"edge {name!r} defined more than once")
        self.edges[name] = (QQbar(vector), True)
    def unsigned_edge(self, name, vector):
        if name in self.edges:
            raise ValueError(f"edge {name!r} defined more than once")
        self.edges[name] = (QQbar(vector), False)
    def define_tile(self, name, outline, orientation=('turn', 1),
                    colour=None, arrow=None):
        if name in self.tiles:
            raise ValueError(f"tile {name!r} defined more than once")
        for element in outline:
            if element[0] not in {'edge', 'turn'}:
                raise ValueError(f"unrecognised element {element!r} "
                                 f"in outline of tile {tile}")
        if orientation[0] != 'turn':
                raise ValueError(f"unrecognised orientation {orientation!r} "
                                 f"for tile {tile}")
        self.tiles[name] = outline, orientation
        self.tile_edges[name] = list(edge_types(outline))
        if colour is not None:
            self.tile_colours[name] = colour
        if arrow is not None:
            self.tile_arrows[name] = arrow
    def edge(self, name, sign=0):
        _, signed = self.edges[name]
        valid_signs = {-1, +1} if signed else {0}
        if sign not in valid_signs:
            raise ValueError(f"edge {name!r} used with illegal sign")
        return ('edge', name, sign)
    def left(self, fraction):
        return ('turn', QQbar(
            UniversalCyclotomicField().gen(fraction.denom())^fraction.numer()))
    def right(self, fraction):
        return self.left(-fraction)
    def define_layer(self, name, tiles, successor):
        if name in self.layers_by_name:
            raise ValueError(f"layer name {name!r} already used")
        layer = Layer(name, set(tiles), successor)
        self.layers_by_name[name] = layer
        self.layers.append(layer)
    def _find_expansion(self, parentlayername, childlayername):
        parentlayer = self.layers_by_name[parentlayername]
        childlayer = self.layers_by_name[childlayername]
        if childlayer.successor != parentlayer.name:
            raise ValueError(f"no expansion should exist from layer "
                             f"{parentlayername} to {childlayername}")
        return self.expansions.setdefault(
            (parentlayername, childlayername),
            self.Expansion(self, parentlayer, childlayer))
    def expand_edge(self, parentlayername, childlayername, signed_edge,
                    outline):
        expansion = self._find_expansion(parentlayername, childlayername)
        outline = list(outline)
        expansion.set_edge(signed_edge, outline)
        reversed_edge = (signed_edge[0], -signed_edge[1])
        if reversed_edge != signed_edge:
            if reversed_edge in expansion.edges:
                raise ValueError(
                    f"edge expansions given for both signs of {signed_edge[0]} "
                    f" ({parentlayername} to {childlayername})")
            expansion.set_edge(reversed_edge, reverse_outline(outline))
    def expand_tile(self, parentlayername, childlayername, parenttile,
                    childindex, childtile, existing_edge, child_edge,
                    colour=None):
        expansion = self._find_expansion(parentlayername, childlayername)
        if parenttile not in expansion.parentlayer.tiles:
            raise ValueError(
                f"tile expansion given for {parenttile} in {parentlayername} "
                f"where it doesn't appear")
        if childtile not in expansion.childlayer.tiles:
            raise ValueError(
                f"tile expansion given for {childtile} in {childlayername} "
                f"where it doesn't appear")
        parentexpansion = expansion.tiles[parenttile]
        if childindex in parentexpansion:
            raise ValueError(
                f"duplicate tile expansion given for child {childindex} "
                f"of {parenttile} in {parentlayername}")
        parentexpansion[childindex] = childtile, existing_edge, child_edge
        expansion.permitted_parents[childtile].add((parenttile, childindex))
        if colour is not None:
            expansion.child_colours[parenttile][childindex] = colour
    def expansion_is_reversing(self, parentlayername, childlayername):
        expansion = self._find_expansion(parentlayername, childlayername)
        expansion.is_reversing = True

    # API for using it afterwards

    def tile_outline(self, tile):
        return self.tiles[tile]

    def polygon_from_outline(self, outline, orientation=None, turtle=None):
        if turtle is None:
            turtle = util.Turtle()
        if orientation is not None:
            turtle.turn(orientation[1])
        vertices = []
        mindepth = collections.defaultdict(lambda: infinity)
        edgelabels = {}
        edgelabel = [0]
        depth = 0
        for element in outline:
            index = len(vertices)
            mindepth[index] = min(depth, mindepth[index])
            if element[0] == 'turn':
                turtle.turn(element[1])
            elif element[0] == 'edge':
                edgelabels[index] = tuple(edgelabel)
                edgelabel[-1] += 1
                vertices.append(turtle.pos)
                turtle.move(self.edges[element[1]][0])
            elif element[0] == 'push':
                depth += 1
                edgelabel.append(0)
            elif element[0] == 'pop':
                depth -= 1
                edgelabel.pop()
                edgelabel[-1] += 1
        return util.Polygon(
            vertices, {i for i, d in mindepth.items() if d > 0}, edgelabels)

    def tile_polygon(self, tile):
        outline, orientation = self.tile_outline(tile)
        return self.polygon_from_outline(outline, orientation)

def load_tiling(fh, params={}):
    tiling = Tiling()

    glob = {
        'signed_edge': tiling.signed_edge,
        'unsigned_edge': tiling.unsigned_edge,
        'define_tile': tiling.define_tile,
        'edge': tiling.edge,
        'left': tiling.left,
        'right': tiling.right,
        'define_layer': tiling.define_layer,
        'expand_edge': tiling.expand_edge,
        'expand_tile': tiling.expand_tile,
        'expansion_is_reversing': tiling.expansion_is_reversing,
        'parameter': params.get,
    }

    exec('from sage.all_cmdline import *', glob)
    exec(preparse(fh.read()), glob)

    tiling.display_coord = glob.get("display_coord", str)

    return tiling
