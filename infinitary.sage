# -*- python -*-

import unittest

def minimise_rho(initial, repeat):
    """Take an eventually periodic string described as an initial segment
    followed by a repeating segment, and return a minimal
    representation of the same string.

    Input parameters 'initial' and 'repeat' can be elements of any
    sequence type, as long as the elements of the sequence are
    hashable. They are understood to represent the infinite string
    consisting of one copy of 'initial' followed by infinitely many
    copies of 'repeat'. Therefore, 'repeat' must be non-empty.

    The return value is a tuple of two sequences, representing the
    same infinite string in the same form, but guaranteed to be its
    minimal representation out of all the possible ones.
    """

    ilen = len(initial)
    rlen = len(repeat)
    assert rlen > 0, "A length-0 repeat doesn't represent an infinite string"

    # Make a state machine that outputs the string, consisting of one
    # state for each position in each string. trans[state] gives the
    # pair (output, next state).
    trans = []
    for i in range(ilen):
        trans.append((initial[i], i+1))
    for i in range(rlen):
        trans.append((repeat[i], ilen + (i+1) % rlen))

    # Assign each state an equivalence class, initially all the same.
    # Then repeatedly refine the equivalence relation by things we can
    # distinguish.
    equiv = [0] * len(trans)
    while True:
        old_equiv = equiv.copy()

        new_equiv = []
        equiv_map = {}
        for state, (output, dest) in enumerate(trans):
            # Distinguish based on the equivalence class of this
            # state, the output symbol on this transition, and the
            # equivalence class of the destination state.
            e = (equiv[state], output, equiv[dest])
            if e not in equiv_map:
                equiv_map[e] = len(equiv_map)
            new_equiv.append(equiv_map[e])
        equiv = new_equiv

        if old_equiv == equiv:
            break

    # Now look for the first repeated class. Since classes are
    # constructed to increment along the string, this is just a matter
    # of finding the first class number less than its index.
    for i, c in enumerate(equiv):
        if c < i:
            # The initial component is up to c, and the repeating
            # component is from c to i.
            s = initial + repeat
            return s[:c], s[c:i]

    # If we didn't find any optimisation, return the original pair.
    return initial, repeat

class EventuallyPeriodicAddress:
    def __init__(self, transducer, initial, repeat):
        self.transducer = transducer
        self.initial, self.repeat = minimise_rho(initial, repeat)
        # First symbol is always special, because it has no parent
        # index, so we don't have to handle the special case of a
        # length-0 repeated segment
        assert len(initial) > 0, "Where's the special first symbol gone?"

    def clone(self):
        # Use __new__ to make an empty instance that hasn't had
        # __init__ run on it, to avoid re-running the minimiser
        copy = object.__new__(type(self))
        copy.transducer = self.transducer
        copy.initial, copy.repeat = self.initial.copy(), self.repeat.copy()
        return copy

    def step(self, edge, boundary=lambda:None):
        in_initial, in_repeat = self.initial.copy(), self.repeat
        # Add the edge index to the initial symbol
        in_initial[0] = (in_initial[0], edge)
        # Feed it to the transducer
        accepted = [False]
        def accept():
            accepted[0] = True
        out_initial, out_repeat = self.transducer.map_repeating_string(
            in_initial, in_repeat, accept)
        # Call the optional notification if we had a boundary
        if not accepted[0]:
            boundary()
        # Pull the edge index off again
        out_initial = list(out_initial)
        out_repeat = list(out_repeat)
        if len(out_initial) == 0:
            out_initial.append(out_repeat[0])
            out_repeat = out_repeat[1:] + out_repeat[:1]
        out_initial[0], out_edge = out_initial[0]
        # Update ourself in place
        self.initial, self.repeat = minimise_rho(out_initial, out_repeat)
        # And return the output edge number
        return out_edge

    def __str__(self):
        return " ".join(list(map(repr, self.initial)) +
                        ["{{{}}}*".format(" ".join(map(repr, self.repeat)))])

    @property
    def base_type(self):
        return self.initial[0]

    @property
    def first_parent(self):
        return (self.initial + self.repeat)[1]

    @property
    def infinite_supertile(self):
        # Characterise the infinite supertile that this coordinate
        # sequence is a part of.
        #
        # Two coordinate sequences represent tiles in the same
        # infinite supertile if and only if they are eventually equal.
        # In this (initial, repeat) representation, that means that if
        # you unroll them to have the same length of initial segment,
        # then their repeating segments are equal.
        #
        # A canonical identifier for the supertile is therefore
        # obtained by unrolling the initial segment until its length
        # is a _multiple_ of the repeat, so that the repeating segment
        # starts at a multiple of its own length.
        #
        # (Note that in some tilings the _same_ infinite supertile can
        # appear multiple times - symmetric instances of Penrose, hats
        # and spectres all exist with this property. So if two
        # coordinate sequences return the same supertile id from this
        # function, it doesn't follow that the _geometric_ positions
        # of those tiles are necessarily within the same supertile
        # region of the plane. Only that the same supertile contains
        # tiles with both of these coordinates.)
        split = (-len(self.initial)) % len(self.repeat)
        return self.repeat[split:] + self.repeat[:split]
